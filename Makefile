# Shell
SHELL		= /bin/sh

# Location of the CUDA Toolkit
CUDA_PATH	?= /usr/local/cuda-7.0

# Architecture & OS
HOST_ARCH	:= $(shell uname -m)
HOST_OS		:= $(shell uname -s 2>/dev/null | tr "[:upper:]" "[:lower:]")
TARGET_ARCH	?= $(HOST_ARCH)
TARGET_OS	?= $(HOST_OS)
TARGET_SIZE	:= 
include Makefile.host
$(info HOST computer: OS $(TARGET_OS); architecture $(TARGET_ARCH))

# Host compiler & flags
HOST_COMPILER	?= g++
NVCC		:= $(CUDA_PATH)/bin/nvcc -ccbin $(HOST_COMPILER) -D_GNU_SOURCE
#debug -Wall -D __LSNS_DEBUG__
CCDEF		:= -D __LINUX__ -D __OPENMP__ -D __LSNS_DEBUG__ 
CCFLAGS		:= -std=c++11 
LDFLAGS		:= -lm
BUILD_TYPE	:=
include Makefile.flags

# Common includes and paths for project
NAME		= lsnsim
OUTDIR		:= ./bin
SRCDIR		= src
SUBDIRS		= $(SRCDIR) $(SRCDIR)/assembler $(SRCDIR)/core
INCLUDES	:= 
LIBRARIES	:= 

# Build the project
CUDA_DETECT	:= 1
BUILD_ENABLED	:= 1
CC		:=
GENCODE_FLAGS	:= 
ALL_CCFLAGS	:=
ALL_LDFLAGS	:=

ifeq ($(CUDA_DETECT),1)
include Makefile.cuda
endif

ifeq ($(BUILD_ENABLED),0)
# Use no compiler
	EXEC		?= @echo "[@]"
$(info Debugging Makefile)
else ifeq ($(BUILD_ENABLED),1)
# Use default compiler
	CC		:= $(HOST_COMPILER)
	ALL_CCFLAGS	+= $(CCDEF) $(CCFLAGS) -O2 -falign-functions=16 -falign-loops=16 -fopenmp
	ALL_LDFLAGS	+= $(LDFLAGS)
	LIBRARIES	+= -fopenmp -lpthread
$(info CUDA driver is not installed properly. Use $(CC) compiler)
else ifeq ($(BUILD_ENABLED),2)
# Use CUDA compiler
	CC		:= $(NVCC)
	CCDEF		+= -D __CUDA__
	NVCCFLAGS	:= -m${TARGET_SIZE} --use_fast_math -arch=sm_35 -rdc=true
	GENCODE_FLAGS	:= -gencode arch=compute_35,code=compute_35
	ALL_CCFLAGS	+= $(CCDEF) $(CCFLAGS) $(NVCCFLAGS)
#	ALL_CCFLAGS	+= $(addprefix -Xcompiler ,$(CCFLAGS))
	ALL_LDFLAGS	+= $(addprefix -Xlinker ,$(LDFLAGS))
$(info CUDA driver is installed. Use $(CC) compiler)
endif

.PHONY: all compile build clean

all: compile build

export EXEC SHELL ALL_CCFLAGS CC BUILD_ENABLED
# Target rules
compile:
	@for i in $(SUBDIRS); do \
		( cd $$i; $(MAKE) compile; cd ../.. );\
	done

build: $(NAME)

OBJECTS		:=
include src/Makefile.list
include src/core/Makefile.list
include src/assembler/Makefile.list
OBJOUT          = $(patsubst %,$(OUTDIR)/%,$(OBJECTS))

$(NAME): $(OBJOUT)
	$(EXEC) $(CC) $(ALL_LDFLAGS) $(GENCODE_FLAGS) -o $@ $+ $(LIBRARIES)

run: build
	$(EXEC) ./lsnsim

clean:
	@for i in $(SUBDIRS); do \
		( cd $$i; $(MAKE) clean; cd ../.. ); \
	done
	rm -f $(NAME)
