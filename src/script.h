﻿#pragma once

#include "nprojects.h"
#include "engine.h"
#include <algorithm>
#include <functional>
#include <map>
#include <set>
#include <string>
#include <vector>
#include <utility>

#ifndef __SCRIPT_H
#define __SCRIPT_H
enum _data_location_{
	LSNS_DEV = 0x1,
	LSNS_HOST = 0x2,
};
///////////////////////////////////////////////////////////////////////////////
// class _netmap
class _netmap{
	protected:
		class dmap{
			public: // constructors/destructor
				dmap( void ) : DataLUT( NULL ), Data( NULL ){ clear(); };
				~dmap( void ){ clear(); };
			public: // operators
				float &operator[]( size_t index ){ return Data_[index]; };
			public: // methods
				size_t size( void ) const{ return Cnt_; };
				size_t size4( void ) const{ return ( Cnt_ > 0 && Cnt_%4 != 0 )? Cnt_/4+1: Cnt_/4; };
				size_t max_size( void ) const{ return MaxSize_; };
			public: // methods
				bool set_base( float4 *base )
				{
					Base = base;
					return false;
				};
				bool push_back( float dat, __lsns_uint32 lut )
				{
					if( Cnt_ > MaxSize_-1 ){
						return false;
					}
					Data_[Cnt_] = dat;
					LUT_[Cnt_] = lut;
					++Cnt_;
					return true;
				};
				bool alloc( __lsns_uint32 size )
				{
					if( size == 0 ){
						return false;
					}
					clear();
					__lsns_uint32 sz = ( size > 0 && size%4 != 0 )? size/4+1: size/4;
					Data = ( float4 *)malloc( sizeof( float4 )*sz ); __lsns_assert( Data != NULL );
					DataLUT = ( uint4 * )malloc( sizeof( uint4 )*sz ); __lsns_assert( DataLUT != NULL );
					MaxSize_ = sz*4;
					return reset();
				};
				bool reset( void )
				{
					if( MaxSize_ > 0 ){
						Cnt_ = 0;
						Data_ = ( float * )Data;
						LUT_ = ( __lsns_uint32 * )DataLUT;
						memset( DataLUT, -1, sizeof( uint4 )*MaxSize_/4 );
						memset( Data, 0, sizeof( float4 )*MaxSize_/4 );
						return true;
					}
					return false;
				};
				void clear( void )
				{
					if( DataLUT ){
						free( DataLUT ); DataLUT = NULL;
					}
					if( Data ){
						free( Data ); Data = NULL;
					}
					MaxSize_ = Cnt_ = 0;
					LUT_ = NULL; Data_ = NULL; Base = NULL;
				};
			public: // data
				float4 __lsns_align( 16 ) *Base;		// Basic address of unfragmented array
				uint4 __lsns_align( 16 ) *DataLUT;		// Look-up-table
				float4 __lsns_align( 16 ) *Data;		// Unfragmented array
			public: // 
				float *Data_;
				__lsns_uint32 *LUT_;
				__lsns_uint32 Cnt_;
				__lsns_uint32 MaxSize_;
		};
	public:
		_netmap( void ) : BaseName(""){};
virtual		~_netmap( void ){};
	public:
		float &operator[]( size_t index ){ return DMap[index]; };
	public:
		size_t size( void ) const{ return DMap.size(); };
	public: // manipulations with local unfragmented array used for i/o operations with network data
		bool init_data( const netdat *data )
		{
			if( DMap.alloc( __lsns_uint32( validate( data ))) ){
				return set_base(( netdat * )data)&&copy_data(( netdat * )data, true );
			}
			return false;
		};
		bool reset_data( const netdat *data )
		{
			return validate( data )&&copy_data(( netdat * )data, true );
		};
template<class mod_> bool modify_data( mod_ &m ){ return m.modify_data( DMap.Data, DMap.size4() ); };
	public: // virtual methods
virtual		bool merge( const _netmap &map ){ return false; };
virtual		bool import_data( const netdat *data ) // import simulation data to unfragmented array DMap
		{ 
			if( DMap.Base != NULL && DMap.size() > 0 ){
				lsns_copy2host( DMap.Base, DMap.Data, DMap.DataLUT, DMap.size4() );
				return true;
			}
			return false;
		};
virtual		bool export_data( const netdat *data )  // export unfragmented array DMap to simulation data
		{
			if( DMap.Base != NULL && DMap.size() > 0 ){
				lsns_copy2dev( DMap.Base, DMap.Data, DMap.DataLUT, DMap.size4() );
				return true;
			}
			return false;
		};
virtual		bool copy_data( netdat *data, bool direction = true ){ return false; }; // copy to(true)/from(false) unfragmented array DMap from/to network data
	protected:
virtual		bool set_base( const netdat *data ){ return false; };
virtual		size_t validate( const netdat *data ){ return 0; };
	protected:
		std::string BaseName;		// Name of basic address of unfragmented array
		dmap DMap;			// i/o unfragmented array of data allocated in local memory
};
///////////////////////////////////////////////////////////////////////////////
// class pmap
class parmap : public _netmap{
	public: // type definitions
		typedef __lsns_uint32 pos;
		typedef std::map<pos, float> pmap;
		typedef pmap::iterator pit;
	public: // constructors/destructor
		parmap( void ) : Bank( -1 ){};
		~parmap( void ){};
	public: // methods
		size_t size( void ) const{ return PMap.size(); };
		bool create( nns_project &prj, const char *unit_name, const char *par_name );
template<class mod_> bool modify( mod_ &m ){ return m.modify( *this ); };
	public: // overrided methods
		bool copy_data( netdat *data, bool direction = true );
	protected:
		bool set_base( const netdat *data );
		size_t validate( const netdat *data );
	public:
		pos Bank;
		pmap PMap;
};
///////////////////////////////////////////////////////////////////////////////
// class wmap
class wmap : public _netmap{
	public: // type definitions
		typedef __lsns_uint32 pos;
		typedef std::pair<pos, float> con;		// pair<position in the array of sources (cells and units),weight of connection>
		typedef std::multimap<pos, con> wsmap;		// [syn_pos; src_pos+weight]
		typedef wsmap::iterator wsit;
	public: // constructors/destructor
		wmap( void ){};
		~wmap( void ){};
	public: // methods
		size_t size( void ) const{ return WMap.size(); };
		bool create( nns_project &prj, const char *trg_name, const char *src_name, const char *syn_name );
template<class mod_> bool modify( mod_ &m ){ return m.modify( *this ); };
	public: // overrided methods
		bool set_base( const netdat *data );
		bool copy_data( netdat *data, bool direction = true );
	protected:
		size_t validate( const netdat *data );
	public: // data
		wsmap WMap;
};
///////////////////////////////////////////////////////////////////////////////
// class val_mod (to modify wmap)
class val_mod{
	public: // constructors/destructor
		val_mod( void ): V( 0.f ){};
		val_mod( float v ): V( v ){};
		~val_mod(){};
	public: // methods
		bool modify( wmap &m )
		{
			for( wmap::wsit it = m.WMap.begin(); it != m.WMap.end(); ++it ){
				it->second.second = V;
			}
			return true; 
		};
		bool modify( parmap &p )
		{
			for( parmap::pit it = p.PMap.begin(); it != p.PMap.end(); ++it ){
				it->second = V;
			}
			return true; 
		};
		bool modify_data( float4 *data, size_t size )
		{
			for( size_t i = 0; i < size; ++i ){
				data[i].x = data[i].y = data[i].z = data[i].w = V;
			}
			return true; 
		};
	public: // data
		float V;
};
///////////////////////////////////////////////////////////////////////////////
// class delta_mod (to modify wmap)
class delta_mod{
	public: // constructors/destructor
		delta_mod( void ): Pos( 0 ), V( 0.f ){};
		delta_mod( size_t p, float v ): Pos( p ), V( v ){};
		~delta_mod(){};
	public: // methods
		bool modify( wmap &m )
		{
			size_t curr_pos = 0;
			for( wmap::wsit it = m.WMap.begin(); it != m.WMap.end(); ++it, ++curr_pos ){
				if( curr_pos == Pos ){
					it->second.second = V;
					return true;
				}
			}
			return false;
		};
		bool modify( parmap &p )
		{
			size_t curr_pos = 0;
			for( parmap::pit it = p.PMap.begin(); it != p.PMap.end(); ++it, ++curr_pos ){
				if( curr_pos == Pos ){
					it->second = V;
					return true;
				}
			}
			return false; 
		};
		bool modify_data( float4 *data, size_t size )
		{
			size_t siz_ = 4*size;
			float *dat_ = ( float * )data;
			for( size_t i = 0; i < siz_; ++i ){
				if( i == Pos ){
					dat_[i] = V;
					return true;
				}
			}
			return false; 
		};
	public: // data
		size_t Pos;
		float V;
};
///////////////////////////////////////////////////////////////////////////////
// class rnd_mod (to modify wmap)
class rnd_mod{
	public: // constructors/destructor
		rnd_mod( void ): X( 0.f ), Var( 0.f ){};
		rnd_mod( float x, float v ): X( x ), Var( v ){};
		~rnd_mod(){};
	public: // methods
		bool modify( wmap &m )
		{
			for( wmap::wsit it = m.WMap.begin(); it != m.WMap.end(); ++it ){
				it->second.second = float( normrd( Rng, X, Var ));
			}
			return true; 
		};
		bool modify_data( float4 *data, size_t size )
		{
			size_t siz_ = 4*size;
			float *dat_ = ( float * )data;
			for( size_t i = 0; i < siz_; ++i ){
				dat_[i] = float( normrd( Rng, X, Var ));
			}
			return true; 
		};
	public: // data
		float X;
		float Var;
};
///////////////////////////////////////////////////////////////////////////////
// class addrnd_mod (to modify wmap)
class addrnd_mod{
	public: // constructors/destructor
		addrnd_mod( void ): Var( 0.f ){};
		addrnd_mod( float v ): Var( v ){};
		~addrnd_mod(){};
	public: // methods
		bool modify( wmap &m )
		{
			for( wmap::wsit it = m.WMap.begin(); it != m.WMap.end(); ++it ){
				it->second.second = float( normrd( Rng, it->second.second, Var ));
			}
			return true; 
		};
		bool modify_data( float4 *data, size_t size )
		{
			size_t siz_ = 4*size;
			float *dat_ = ( float * )data;
			for( size_t i = 0; i < siz_; ++i ){
				dat_[i] = float( normrd( Rng, dat_[i], Var ));
			}
			return true; 
		};
	public: // data
		float Var;
};

#endif /* __SCRIPT_H */
