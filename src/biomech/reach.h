#include "biomech.h"

#ifndef __REACH_H
#define __REACH_H

extern void elbow( float *e, float x, float y );
extern void reach( float *x, float *y, float dt =.001f );
extern bool reach_init( void );

#endif /*__REACH_H*/
