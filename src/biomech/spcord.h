#include "muscles.h"

#ifndef __SPCORD_H
#define __SPCORD_H

const float EpsY = 1e-05f;;
const float SPN_V12 = 0.5f;
const float SPN_Slope = 0.15f;
const float Eleak = -0.28f; //default bias 

extern void sp_cord( float fb_Ia[NUM_MUSCLES], float fb_Ib[NUM_MUSCLES], float drive[NUM_MUSCLES], float mn[NUM_MUSCLES] );

#endif /*__SPCORD_H*/
