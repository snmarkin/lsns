#include "reach.h"
#include "muscles.h"
#include "spcord.h"

const float D1 = L1/2.f;                // %m, center mass (upper segment)
const float D2 = L2/2.f;                // %m, center mass (lower segment)
const float I1 = (M1*L1*L1)/3.f;        // %moment of inertia, about fixed end (upper segment)
const float I2 = (M2*L2*L2)/12.f;       // %moment of inertia, about axis through center of mass (lower segment)
const float L1g = 0.f;                  // %projection L1 onto gravity vector
const float D1g = 0.f;                  // %projection D1 onto gravity vector
const float D2g = 0.f;                  // %projection D2 onto gravity vector

void DF_Inv( float t, float *xdot, float *x, float dr[NUM_MUSCLES] )
{
static	float l[NUM_MUSCLES] = {0.f}, v[NUM_MUSCLES] = {0.f};
static	float hup[NUM_MUSCLES] = {0.f}, hdw[NUM_MUSCLES] = {0.f};
static	float mn[NUM_MUSCLES] = {0.f}, f[NUM_MUSCLES] = {0.f};
static	float fbIa[NUM_MUSCLES] = {0.f}, fbIb[NUM_MUSCLES] = {0.f}, fbII[NUM_MUSCLES] = {0.f};

	// joint specific angles and angular velocities 
	float theta[2] = { x[0], x[1]-x[0] };
	float thetaV[2] = { x[2], x[3]-x[2] };

	m_geometry( theta, thetaV, l, v, hup, hdw );
	m_feedback( l, v, mn, f, fbIa, fbIb, fbII );
	sp_cord( fbIa, fbIb, dr, mn );
	m_force( mn, l, v, f );
	// calculting torques from forces
	float q1m = R[0]*f[0]+R[1]*f[1]+R[4]*f[4]+R[5]*f[5];
	float q2m = R[2]*f[2]+R[3]*f[3]+R[6]*f[4]+R[7]*f[5];
	// specify joint restrictions
	float a1min = M_PI/2-A1max;
	float a1max = M_PI/2-A1min;
	float a2min = M_PI-A2max;
	float a2max = M_PI-A2min;
	//---- calculating total non-conservative forces 
	// 1. joints viscosity
	float q1v = -BJ*thetaV[0]; 
	float q2v = -BJ*thetaV[1];
	// 2. joints restrictions (elastic and viscosity components)
	float q1e = 0.f;
	float q2e = 0.f;
	if( theta[0] > a1max && thetaV[0] > 0 ){       // if shoulder flexion exceeds the limit
		q1e = -KJr*(theta[0]-a1max)-BJr*thetaV[0];
	}
	else if( theta[0] < a1min && thetaV[0] < 0.f ){ // if shoulder extension exceeds the limit
		q1e = -KJr*(theta[0]-a1min)-BJr*thetaV[0];
	}

	if( theta[1] > a2max && thetaV[1] > 0.f ){      // if elbow flexion exceeds the limit
		q2e = -KJr*(theta[1]-a2max)-BJr*thetaV[1];
	}
	else if( theta[1] < a2min && thetaV[1] < 0.f ){ // if elbow flexion exceeds the limit
		q2e = -KJr*(theta[1]-a2min)-BJr*thetaV[1];
	}
	// 3. total non-conservative forces 
	float q1 = q1v-q2v+q1e-q2e+q1m-q2m; // upper joint
	float q2 = q2v+q2e+q2m;             // lower joint 
	//---- calculating total conservative forces 
	float f1c = -float( M2*L1*D2*sin(x[0]-x[1])*x[3]*x[3] );
	float f2c = float( M2*L1*D2*sin(x[0]-x[1])*x[2]*x[2] );
	float f1g = float( M1*g*D1g*sin(x[0])+M2*g*L1g*sin(x[0]) );
	float f2g = float( M2*g*D2g*sin(x[1]) );
	//---- calculating total torques in upper and lower joints;
	float Q1 = f1c-f1g+q1; 
	float Q2 = f2c-f2g+q2;
	//--- calculating dynamics of the system
	float a1 = I1+M2*L1*L1;
	float a2 = I2+M2*D2*D2;
	float b = float( M2*L1*D2*cos(x[0]-x[1]));
	float D = a1*a2-b*b;
	xdot[0] = x[2]; // % angle in upper joint
	xdot[1] = x[3]; // % angle in lower joint
	xdot[2] = (a2*Q1-b*Q2)/D; // % angular velocity in upper joint
	xdot[3] = (a1*Q2-b*Q1)/D; // % angular velocity in lower joint
}


#include <iostream>
#include <fstream>

using namespace std;
const int NA = 100, Niter = 1000;
static bool IsInit = false;
static float M1_Dr0[NA][Niter][NUM_MUSCLES]={};
	
bool reach_init( void )
{
	ifstream data( "ini\\cortical_data" );
	if( !data.is_open()){
		data.open( "cortical_data" );
	}
	if( data.is_open() ){
		for( int d = 0; d < NA; ++d ){
			for( int i = 0; i < Niter; i++ ){
				for( int j = 0; j < NUM_MUSCLES; j++ ){ 
					data >> M1_Dr0[d][i][j];
				}
			}
		}
		IsInit = true;
		return true;
	}
	return false;
}

void reach( float *x, float *y, float dt )
{
	float xdot[4];
	float m1[NUM_MUSCLES] = {0.};
	for( int i = 0; i < Niter; ++i ){
		for( int k = 0; k < NUM_MUSCLES; ++k ){
			m1[k] = M1_Dr0[0][0][k];
			for( int j = 0; j < NA; j++ ){
				m1[k] += y[j]*( M1_Dr0[j][i][k]-M1_Dr0[0][0][k] );
			}
		}
		float t = i*dt;
		DF_Inv( t, xdot, x, m1 );
		for( int j = 0; j < 4; j++ ){
			x[j]+=xdot[j]*dt;
		}
	}
}

void elbow( float *e, float x, float y )
{
	// % Calculate the position of the elbow (xe, ye) and the two angles
	float a = x*x+y*y;
	float q = L1*L1-L2*L2+a;
	
	if( fabs( y ) > fabs( x )){
		float B = -x*q;
		float C = 1.f/4.f*q*q-L1*L1*y*y;
		float D = B*B-4.f*a*C;
		e[0] = ( y < 0.f )? float( ( -B-sqrt(D))/a/2.f ): float( (-B+sqrt(D))/a/2.f );
		e[1] = ( q-2.f*x*e[0] )/y/2.f;
	}
	else
	{
		float B = -y*q;
		float C = 1.f/4.f*q*q-L1*L1*x*x;
		float D = B*B-4.f*a*C;
		e[1] = ( x > 0.f )? float( (-B-sqrt(D))/a/2.f ): float( (-B+sqrt(D))/a/2.f );
		e[0] = ( q-2.f*y*e[1] )/x/2.f;
	}
}
