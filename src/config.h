#pragma once

#ifndef __LSNS_CONFIG_H
#define __LSNS_CONFIG_H

#if !defined( __OPENMP__ ) /*check if macros defined in the command line*/
	#define __OPENMP__
#endif /* __OPENMP__ */
#if !defined( __CONSOLE__ ) /*check if macros defined in the command line*/
	#define __CONSOLE__
#endif /* __CONSOLE__ */
#if !defined( __GUI__ ) /*check if macros defined in the command line*/
//	#define __GUI__
#endif /* __GUI__ */
#if !defined( __WINDOWS__ ) /*check if macros defined in the command line*/
	#define __WINDOWS__
#endif /* __WINDOWS__ */
#if !defined( __LINUX__ ) /*check if macros defined in the command line*/
//	#define __LINUX__
#endif /* __LINUX__ */
#if !defined( __CUDA__ ) /*check if macros defined in the command line*/
	#define __CUDA__
#endif /* __CUDA__ */
#if !defined( __LSNS_DEBUG__ ) /*check if macros defined in the command line*/
//	#define __LSNS_DEBUG__
#endif /* __LSNS_DEBUG__ */
#if !defined( __DATA_OUT__ ) /*check if macros defined in the command line*/
// uncomment the line below to redirect the results to stderr pipeline (the results of simulations won't store to RAM)
//	#define __DATA_OUT__
#endif /* __CONSOLE__ */

#if defined( __OPENMP__ )
	#include <omp.h>
#endif /* __OPENMP__ */

#if defined( __LINUX__ )
	#undef __WINDOWS__
#elif defined( __WINDOWS__ )
	#undef __LINUX__
#else
	#define __WINDOWS__
	#undef __LINUX__
#endif /* __LINUX__ | __WINDOWS__ */

#if defined( __GUI__ )
	#undef __CONSOLE__
#elif defined( __CONSOLE__ )
	#undef __GUI__
#else
	#define __CONSOLE__
	#undef __GUI__
#endif /* __GUI__ | __CONSOLE__ */

#if defined( __CUDACC__ ) || defined ( __CUDA__ ) // NVCC | CUDA
	#include <cuda_runtime.h>
	#include <cuda.h>

	#define __lsns_inline __forceinline__ __device__
	#define __lsns_global __global__
	#define __lsns_constant __device__ __constant__
	#define __lsns_align( n ) __align__( n )
	#if __CUDA_ARCH__ >= 350
		#define __lsns_cached( p ) __ldg( &p )
	#else
		#define __lsns_cached( p ) p
	#endif
	#define __lsns_ithread( i ) int i = threadIdx.x+blockIdx.x*blockDim.x+blockIdx.y*blockDim.x*gridDim.x
	typedef int __lsns_int32;
	typedef unsigned __lsns_uint32;
#elif !defined( __CUDA__ )
	#include <xmmintrin.h>

	#define __lsns_inline inline
	#define __lsns_global static
	#define __lsns_constant static
	#define __lsns_cached( p ) p
	#define __lsns_ithread( i )
	#if defined( __GNUC__ ) /* GCC */
		#include <stdint.h>
		#define __lsns_align( n ) __attribute__(( aligned( n )))
		typedef int32_t __lsns_int32;
		typedef uint32_t __lsns_uint32;
	#elif defined( _MSC_VER ) /* MSVC */
		#define __lsns_align( n ) __declspec( align( n ))
		typedef __int32 __lsns_int32;
		typedef unsigned __int32 __lsns_uint32; 
	#else
		#define __lsns_align( n )
		typedef int __lsns_int32;
		typedef unsigned int __lsns_uint32; 
		#error "Please, provide a definition for __lsns_align macro for your host compiler!"
		#error "Please, provide a definition for __lsns_int32 macro for your host compiler!"
		#error "Please, provide a definition for __lsns_uint32 macro for your host compiler!"
	#endif
	typedef union __int2{
		__m64 __i4;
		struct{
			__lsns_int32 x;
			__lsns_int32 y;
		};
	} int2;
	typedef union __uint2{
		__m64 __i4;
		struct{
			__lsns_uint32 x;
			__lsns_uint32 y;
		};
	} uint2;
	typedef union __int4{
		__m128 __i4;
		struct{
			__lsns_int32 x;
			__lsns_int32 y;
			__lsns_int32 z;
			__lsns_int32 w;
		};
	} int4;
	typedef union __uint4{
		__m128 __i4;
		struct{
			__lsns_uint32 x;
			__lsns_uint32 y;
			__lsns_uint32 z;
			__lsns_uint32 w;
		};
	} uint4;
	typedef union __float2{
		__m64 __f2;
		struct{
			float x;
			float y;
		};
	} float2;
	typedef union __float4{
		__m128 __f4;
		struct{
			float x;
			float y;
			float z;
			float w;
		};
	} float4;
	typedef struct __dim3{
		__lsns_uint32 x;
		__lsns_uint32 y;
		__lsns_uint32 z;
	} dim3;
#endif /* __CUDACC__ | __GNUC__ | _MSC_VER  */
#endif /*__LSNS_CONFIG_H*/

