#pragma once

#ifndef __LSNS_CUDATIMER_H
#define __LSNS_CUDATIMER_H

#include "config.h"
#include "debug.h"

#if defined  (__CUDA__ )

#include <cuda.h>

///////////////////////////////////////////////////////////////////////////////
// class 'cuda_timer'
// Input parameters:
// Output parameters:
class cuda_timer{
	public: // constructors/destructor
		cuda_timer( void ) : IsStarted( false ){
			cudaError_t e = cudaEventCreate( &Start ); __lsns_assert(  e == cudaSuccess );
			e = cudaEventCreate( &Stop ); __lsns_assert(  e == cudaSuccess );
		};
		~cuda_timer( void ){
			cudaError_t e = cudaEventDestroy( Start ); __lsns_assert(  e == cudaSuccess );
			e = cudaEventDestroy( Stop ); __lsns_assert(  e == cudaSuccess );
		};
	public: // methods
		void start( cudaStream_t s = 0 ){
			if( !IsStarted ){
				cudaEventRecord( Start, s );
				IsStarted = true;
			}
		};
		void stop( cudaStream_t s = 0 ){
			if( IsStarted ){
				cudaEventRecord( Stop, s ); 
				IsStarted = false;
			}
		};
		float elapsed( void ){
			if( IsStarted ){
				return -1.0; 
			}
			cudaEventSynchronize( Stop );
			float elapsed = 0;
			cudaEventElapsedTime( &elapsed, Start, Stop );
			;
			return elapsed;	//msec
		};
	private: // parameters
		bool IsStarted;
		cudaEvent_t Start;
		cudaEvent_t Stop;
};

#endif /* __CUDA__ */

#endif /*__LSNS_CUDATIMER_H*/
