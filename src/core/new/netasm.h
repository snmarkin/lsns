#pragma once

#ifndef __LSNS_NETASM_H
#define __LSNS_NETASM_H

#include "engine.h"

///////////////////////////////////////////////////////////////////////////////
// class wsum: auxiliary class that initialize network structures related to
// 'wsumdat'. It provides minimal functionality to describe the weighted sum 
// of input signals for particular synapse(s) and maps the description to 
// to correspondent network structure. First, to map actual weighted sum onto 
// correspondent memory of network structure the 'resetmap' should be called.
// Then the method 'map2mem' should be called for all synapses.
class wsum{
	public: // type definition
		typedef __lsns_uint32 wstype;			// type of summation
		typedef __lsns_uint32 wspar;			// parameters of summation
		typedef __lsns_pos32 pos;			// position of network element in the global array
		typedef std::pair<pos, wspar> wstrg;		// pair<position in the array of weighted sum (wsumdat), parameters of synaptic summation>
		typedef std::pair<pos, float> con;		// pair<position in the array of sources (cells and units),weight of connection>
		typedef std::vector<wspar> wsmap;		// type of synaptic summation
		typedef std::vector<con> srcmap;		// list of synaptic connections
		typedef size_t hashws;				// hash value of weghted sum 
	public: // constructors/destructor
		wsum( void ) : Type( wstype( -1 )){};
		wsum( const wsum &syn ) : Type( syn.Type ), WSMap( syn.WSMap ), SrcMap( syn.SrcMap ){};
		~wsum( void ){};
	public: // operators
		wsum& operator = ( const wsum &syn ){ 
			Type = syn.Type; WSMap = syn.WSMap; SrcMap = syn.SrcMap; return *this; 
		};
		bool operator == ( const wsum &syn ){
			return  ( Type == syn.Type )&&( SrcMap == syn.SrcMap );
		}
		bool operator == ( const srcmap &src ){
			return SrcMap == src;
		}
	public: // get dimention of specific synapses
		pos size_ws( void ){				// return total number of synapses related to particular weighted sum
			return pos( WSMap.size());
                };
		pos size_src( void ){				// return total number of connections for particular weighted sum
			return pos( SrcMap.size());
                };
		pos size4_src( void ){				// return total number of float4 and uint4 structures to store connections
			pos size = pos( SrcMap.size()); 
			return ( size%4 == 0 )? size/4: ( size/4+1 );
		};
	public: // operations with weighted sum and synapses
		bool create_syn( wstype t, wspar par );		// create synapse(type, parameters of summation)
		bool add_ws( const wspar &ws, bool major );	// add weighed wum
		bool add_src( const srcmap &src );		// add sources to the synapse
	public: // initialize network data related to the synapse
static		void resetmap( void ){ Trg0 = Src0 = 0; };	// reset all variales related to memory map
		wstrg map2mem( netdat *data, synpar *par, std::vector<wstrg> &wsyn ); // map particular synapses and connections to network structure
	private:
		void map_ws2mem( uint4 *src, float4 *w, pos index ); // map 4 consequence weights and sources to correspondent w and src
	public: // data
		wstype Type;			// type of synaptic summation<=>wsumdat.SynType.x (see: synapse.h)
		wsmap WSMap;			// list of similar synapses (the same Type and SrcMap)
		srcmap SrcMap;			// map of synaptic connections (pair of weight and source); SrcMap.size()<=>wsumdat.SynType.z; SrcMap[i]<=>wsumdat.Wall&SrcLUT[srcpos+i/4](x,y,z,w) (see synapse.h)
	private:
static		pos Src0;
static		pos Trg0;
};
///////////////////////////////////////////////////////////////////////////////
// class syntype
class syntype{
	public: // type definition
		typedef __lsns_pos32 wstype;			// type of summation
		typedef __lsns_pos32 wspar;			// parameters of summation
	public: // constructors/destructor
		syntype( void ){};
		syntype( float g, wstype t, wspar p ) : Gmax( g ), Type( t ), Par( p ){};
		syntype( const syntype &t ) : Gmax( t.Gmax ), Type( t.Type ), Par( t.Par ){};
		~syntype( void ){};
	public: // operators
		syntype &operator = ( const syntype &t ){ Gmax = t.Gmax; Type = t.Type; Par = t.Par; return *this; };
	public: // data
		float Gmax;			// maximal conductance
		wstype Type;			// type of synaptic summation
		wspar Par;			// parameters of synaptic summation

};
///////////////////////////////////////////////////////////////////////////////
// class syntrg
class syntrg{
	public: // type definition
		typedef __lsns_pos32 pos;
	public: // constructors/destructor
		syntrg( void ){};
		syntrg( pos s, pos v, pos e ) : Syn( s ), Vm( v ), Eds( e ){};
		syntrg( const syntrg &t ) : Syn( t.Syn ), Vm( t.Vm ), Eds( t.Eds ){};
		~syntrg( void ){};
	public: // operators
		syntrg &operator = ( const syntrg &t ){ Syn = t.Syn; Vm = t.Vm; Eds = t.Eds; return *this; };
	public: // data
		pos Syn;			// position in ChanG
		pos Vm;				// position in CellV/UnitV
		pos Eds;			// position in IonE
};
///////////////////////////////////////////////////////////////////////////////
// class mapsyn 
class mapsyn{
	public: // type definition
		typedef __lsns_pos32 pos;			// position of network element 
		typedef __lsns_pos32 wspar;			// parameters of synaptic summation
		///////////////////////////////////////////////////////////////////////////////
		// class syntrg_  parameters of synapse
		class syntrg_{
			public: // constructors/destructor
				syntrg_( void ){};
				syntrg_( float g, pos s, pos v, pos e ) : Gmax( g ), Syn( s ), Vm( v ), Eds( e ){};
				syntrg_( const syntrg_ &t ) : Gmax( t.Gmax ), Syn( t.Syn ), Vm( t.Vm ), Eds( t.Eds ){};
				~syntrg_( void ){};
			public: // operators
				syntrg_ &operator = ( const syntrg_ &t ){ Gmax = t.Gmax; Syn = t.Syn; Vm = t.Vm; Eds = t.Eds; return *this; };
				bool operator == ( const syntrg_ &t ){ return Syn == t.Syn; };
				bool operator < ( const syntrg_ &t ){ return Syn < t.Syn; };
			public: // data
				float Gmax;			// maximal conductance
				pos Syn;			// position in ChanG
				pos Vm;				// position in CellV/UnitV
				pos Eds;			// position in IonE
		};
		class trgmap_{
			public: // type definition
				typedef std::unordered_multimap<wspar, syntrg_> map_;
			public: // constructors/destructor
				trgmap_( void ){};
				trgmap_( const trgmap_ &t ) : WSum( t.WSum ), TrgMap( t.TrgMap ){};
				~trgmap_( void ){};
			public: // operators
				trgmap_ &operator = ( const trgmap_ &t ){ WSum = t.WSum; TrgMap = t.TrgMap; return *this; };
			public: // data
				wsum WSum;
				map_ TrgMap;
		};
		typedef std::unordered_multimap<wsum::hashws, trgmap_> synmap;// list of synapses for whole network
		typedef synmap::iterator mit;
	public: // constructors/destructor
		mapsyn( void ){};
		mapsyn( const mapsyn &map ){};
		~mapsyn( void ){};
	public: // operators
		mapsyn& operator = ( const mapsyn &map ){
			return *this;
		};
	public: // methods
		pos size_wsum( void ){				// total number of all weighted sums
			pos size = 0;
			for( mit _syn = SynMap.begin(); _syn != SynMap.end(); size += _syn->second.WSum.size_ws(), ++_syn );
			return size;
		};
		pos size_src( void ){				// return total number of connections for all weighted sums
			pos size = 0;
			for( mit _syn = SynMap.begin(); _syn != SynMap.end(); size += _syn->second.WSum.size_src(), ++_syn );
			return size;
                };
		pos size4_src( void ){				// return total number of float4 and uint4 structures to store connections
			pos size = 0;
			for( mit _syn = SynMap.begin(); _syn != SynMap.end(); size += _syn->second.WSum.size4_src(), ++_syn );
			return size;
		};
		void add_synapse( const syntype &syn, const syntrg &trg, wsum::srcmap &src, bool compress = true );
		void map2mem(  netdat *data, synpar *par );
	private:
		void add_newsyn( syntype::wstype type, std::pair<wspar, syntrg_> &trg, const wsum::srcmap &src, wsum::hashws hash );
	public: // data
		synmap	SynMap;			// list of synapses for whole network
};
///////////////////////////////////////////////////////////////////////////////
// class nasm 
class nasm{
	public:
		nasm( netpar &net ) : Net( net ), ChanPos( 0 ), IonPos( 0 ){};
		~nasm( void ){};
	public: // utilities
		///////////////////////////////////////////////////////////////////////////////
		// 'make_epos32' is based on the code provided by Taegyo Kim. 
		// It returns encoded position of particular variable in 'float4' array 
		// 'bank' - position of particular variable in float4 structure: 0-x, 1-y, 2-z, 3-w
		// 'offset' - offset of particular float4 structure in flat4 array
		__lsns_pos32 make_epos32( __lsns_pos32 bank, __lsns_pos32 offset )
		{
			if( offset < 0x3FFFFFFF ){
				bank &= 0x03;
				return ( bank << 30 )|offset; // address up to 4GB-1
			}
			return -1;	// out of range
		};
		///////////////////////////////////////////////////////////////////////////////
		// 'make_epos64' returns encoded position of particular variable in global array 
		// 'bank' - position of particular variable in float4 structure: 0-x, 1-y, 2-z, 3-w
		// 'offset' - offset of particular float4 structure in flat4 array
		__lsns_pos64 make_epos64( __lsns_pos32 bank, __lsns_pos32 offset )
		{
			__lsns_pos64 pos = bank&0x03;
			return ( pos<<32 )|offset; // address up to 16 GBytes
		};
		///////////////////////////////////////////////////////////////////////////////
		// provides access to any network variable allocated on host computer.
		// epos is encoded position of requested variable relative to data array
		float &get_data( __lsns_pos32 epos, float4 *data )
		{
			__lsns_pos32 bank = ( epos>>30 )&0x03;	// bank
			__lsns_pos32 offset = epos&0x3FFFFFFF;	// offset
			return (( float * )( data+offset ))[bank];
		};
		///////////////////////////////////////////////////////////////////////////////
		// provides access to any network variable allocated on host computer.
		// epos is encoded position of requested variable relative to Data->GlobalData array
		float &get_data( __lsns_pos64 epos )
		{
			__lsns_pos32 bank = ( epos>>32 )&0x03;	// bank
			__lsns_pos32 offset = epos&0xFFFFFFFF;	// offset
			return (( float * )( Net.Data->GlobalData+offset ))[bank];
		};
	public:
		///////////////////////////////////////////////////////////////////////////////
		// 'make_ionid'
		__lsns_uint32 make_ionid( __lsns_uint32 etype, __lsns_uint32 ptype, __lsns_uint32 ppar, __lsns_uint32 w = -1 )
		{
			return make_id( etype, ptype, ppar, w );
		};
		///////////////////////////////////////////////////////////////////////////////
		// 'make_chanid'
		__lsns_uint32 make_chanid( __lsns_uint32 typem, __lsns_uint32 parm, __lsns_uint32 typeh, __lsns_uint32 parh )
		{
			return make_id( typem, parm, typeh, parh );
		};
	private:
		__lsns_uint32 make_id( __lsns_uint32 hh, __lsns_uint32 hl, __lsns_uint32 lh, __lsns_uint32 ll )
		{
			return (( hh&0xFF ) << 24 )+(( hl&0xFF ) << 16 )+(( lh&0xFF ) << 8 )+( ll&0xFF );
		};
		__lsns_uint32 get_hihi( __lsns_uint32 id ) // :) high-high
		{
			return ( id&0xFF000000 ) >> 24;  
		};
		__lsns_uint32 get_hilo( __lsns_uint32 id ) // ;) high-low
		{
			return ( id&0x00FF0000 ) >> 16;  
		};
		__lsns_uint32 get_lohi( __lsns_uint32 id ) // :D low-high
		{
			return ( id&0x0000FF00 ) >> 8;
		};
		__lsns_uint32 get_lolo( __lsns_uint32 id ) // :) low-low
		{
			return id&0x000000FF;
		};
	public: // methods
		///////////////////////////////////////////////////////////////////////////////
		// 'get_ionpos' get position of ion relative to iondat array
		__lsns_pos32 get_ionpos( __lsns_uint32 ion_id, __lsns_pos32 unit_pos );
		///////////////////////////////////////////////////////////////////////////////
		// 'get_chanpos' get position of channel relative to chandat array
		__lsns_pos32 get_chanpos( __lsns_uint32 chan_id, __lsns_pos32 ion_pos, __lsns_pos32 unit_pos );
		///////////////////////////////////////////////////////////////////////////////
		// 'get_synpos' get position of synapse relative to chandat array
		__lsns_pos32 get_synpos( __lsns_uint32 syn_id, __lsns_pos32 ion_pos, __lsns_pos32 unit_pos );
		///////////////////////////////////////////////////////////////////////////////
		// 'getw_pos' provides access to any network variable allocated on host computer
		__lsns_pos64 get_wpos( __lsns_uint32 syn_id, __lsns_pos32 ion_pos, __lsns_pos32 trg_pos, __lsns_pos32 src_pos );
		///////////////////////////////////////////////////////////////////////////////
		// 'getw_pos' provides access to any network variable allocated on host computer
		__lsns_pos64 get_wpos( __lsns_uint32 syn_pos, __lsns_pos32 trg_pos, __lsns_pos32 src_pos );
	public: // methods
		///////////////////////////////////////////////////////////////////////////////
		// 'add_ion' get position of ion relative to iondat array
		__lsns_pos32 add_ion( __lsns_uint32 ion_id, __lsns_pos32 unit_pos );
		///////////////////////////////////////////////////////////////////////////////
		// 'add_chan'
		__lsns_pos32 add_chan( __lsns_uint32 chan_id, __lsns_pos32 unit_pos, __lsns_pos32 ion_pos, __lsns_pos32 ion_mpos = -1, __lsns_pos32 ion_hpos = -1 );
		///////////////////////////////////////////////////////////////////////////////
		// 'add_syn'
		__lsns_pos32 add_syn( __lsns_uint32 syn_id, __lsns_pos32 unit_pos, __lsns_pos32 ion_pos );
		///////////////////////////////////////////////////////////////////////////////
		// 'set_gmax'
		bool set_changmax( __lsns_pos32 chan_pos, float gmax );
		///////////////////////////////////////////////////////////////////////////////
		// 'set_mh0'
		bool set_chanmh0( __lsns_pos32 chan_pos, float m0, float h0, float pm, float ph  );
		bool set_ione( __lsns_pos32 ion_pos, float eds, float in, float out, float rtfz );
		bool set_ioni( __lsns_pos32 ion_pos, float ipump, float ichan, float z = -1, float w = -1 );
		bool set_iondyn( __lsns_pos32 ion_pos, __lsns_pos32 chan_pos );
	private: // methods
		///////////////////////////////////////////////////////////////////////////////
		// 'map2lut' modify look-up-table of network elements (cells, drives, outputs etc)
		// which refer to ions currents involved in dynamics of membrane potential
		// 'lut' - array of look-up-tables for all network elements 
		// 'lut_pos' - position of current network element
		// 'lut_ref' - actual number @ lut[lut_pos+counter]
		// 'size' - maximal size of look-up-table
		void map2lut( uint4 *lut, __lsns_uint32 lut_pos, __lsns_uint32 lut_ref, __lsns_uint32 size );
		///////////////////////////////////////////////////////////////////////////////
		// 'init_ion'
		void init_ion( __lsns_uint32 ion_id, __lsns_pos32 ion_pos, __lsns_pos32 unit_pos );
		///////////////////////////////////////////////////////////////////////////////
		// 'init_chan'
		void init_chan( __lsns_uint32 chan_id, __lsns_pos32 chan_pos, __lsns_pos32 unit_pos, __lsns_pos32 ion_pos, __lsns_pos32 ion_mpos, __lsns_pos32 ion_hpos );
	private: // data
		netpar &Net;
		__lsns_pos32 ChanPos;
		__lsns_pos32 IonPos;
		__lsns_pos32 CellPos;
		__lsns_pos32 UnitPos;
};

#endif /*__LSNS_NETASM_H*/
