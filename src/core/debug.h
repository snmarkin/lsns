#pragma once

#ifndef __LSNS_ASSERT_H
#define __LSNS_ASSERT_H

#include "config.h"
#include <assert.h>

#if defined( __LSNS_DEBUG__ )
	#define __lsns_assert( c ) ( assert( c ), 0 )
#else
	#define __lsns_assert( c ) assert( c )
#endif

#endif /*__LSNS_ASSERT_H*/

