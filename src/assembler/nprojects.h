#ifndef _NPROJECTS_H
#define _NPROJECTS_H

#include "myheader.h"
#include "networks.h"
#include "npopulations.h"
#include "iodata.h"
#include "nnonspikeunits.h"

class nns_project {
public:
	nns_project(){};
public:
	
	string name;
	string datapath;
	string nnspath;
	contr_parameters ctr_para;   // TK added
	vector<synp_parameters> syn_para; // TK
	
	vector<neural_population> pops;
	nns_tnet tnet;
	nns_nonspikeunit nsu;
	vector<nns_iodata> iodata;

	void reset();
};

void create_param(vector<parameter_set>&);
void create_proj(vector<nns_project>&, string);
void rename_proj(vector<nns_project>&, string, string);


#endif