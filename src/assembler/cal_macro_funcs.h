#ifndef __CAL_MACRO_FUNCS_H
#define __CAL_MACRO_FUNCS_H

//////////////////////////////////////////////////////////////////////
// Definition of macro functions to calculate all paramaters for I channels
// Macro functions are faster than regular calling functions in calculation
#define POW_2(x) \
	(x)*(x)
#define POW_3(x) \
	POW_2(x)*(x)
#define POW_4(x) \
	POW_3(x)*(x)

#define CAL_TM(Vc,Vhfm,tm_max,ktm) \
	(tm_max) / cosh(((Vc)-(Vhfm))/(ktm))
#define CAL_M_INF(Vc,Vhfm,km) \
	1./(1.+exp(-((Vc)-(Vhfm))/ (km)))
#define CAL_M(m_inf,Mpre,ODEstep,Tm) \
	((m_inf) - (Mpre))*(ODEstep)/(Tm)
#define CAL_TH(Vc,Vhfh,th_max,kth) \
	(th_max) / cosh(((Vc)-(Vhfh))/(kth))
#define CAL_H_INF(Vc,Vhfh,kh) \
	1./(1.+exp(-((Vc)-(Vhfh))/ (kh)))
#define CAL_H(h_inf,Hpre,ODEstep,Th) \
	((h_inf) - (Hpre))*(ODEstep)/(Th)
#define CAL_I(g,powM,powH,Vc,E) \
	(g)*(powM)*(powH)*((Vc)-(E))
#define CAL_IM(g,powM,Vc,E) \
	(g)*(powM)*((Vc)-(E))
#define CAL_IH(g,powH,Vc,E) \
	(g)*(powH)*((Vc)-(E))
#define CAL_ING(g,Vc,E) \
	(g)*((Vc)-(E))
#define ODE_EULER(y0,yr,ODEstep,t) \
	((y0)-(yr))*exp(-(ODEstep)/(t)) + (yr)

#endif