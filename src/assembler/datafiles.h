#ifndef __DATAFILES_H
#define __DATAFILES_H

//#include "myheader_all.h"
#include "myheader.h"
//#include "networks.h"
#include "nprojects.h"

//void save_nns(const vector<neural_population>&, string);
//void save_nns(const <neural_population>&,const vector<nns_net>&, const vector<nns_mnet>&, string);
void save_nns(const nns_project&, string);
void save_nns(const nns_project&);
void save_as_nns(const nns_project&, string);
int load_nns(string,nns_project&);
int load_nns(string,vector<neural_population>&,vector<nns_net>&,vector<nns_mnet>&);
//int parse_nns(vector<string>&,vector<neural_population>&);
int parse_nns(vector<string>&,nns_project&);
//int parse_nns(vector<string>&,vector<neural_population>&,vector<nns_net>&,vector<nns_mnet>&);

vector<int> findlv(vector<string>&,string); //Find lines with vector output
vector<int> findlvw(vector<string>&,int,int,string); //Find lines with vector output within a window

int findlw(vector<string>&,int,int,string); //Find a line within a window
int findl(vector<string>&,string); //Find a line
vector<int> findh(vector<string>&,string,int); //Find header
vector<int> findh(vector<string>&,string);
vector<int> findh(vector<string>&,int,int,string);
vector<int> findh(vector<string>&,int,int,string,int);

int getrmtype(string&);

string extsc(string str, int n);
int extic(string str, int n);
double extdc(string str, int n);
param extpc(string str, int n);
int exti(vector<string>&,int,int,string); //Extract integer
int exti(vector<string>&,string); //Extract integer
double extd(vector<string>&,int,int,string); //Extract double
string exts(vector<string>&,int,int,string); //Extract String
param extp(vector<string>&,int,int,string); //Extract param
vector<int> extli(vector<string>&,int,int,string,int); //Extract multiple integers in a line
vector<string> extls(vector<string>&,int,int,string,int); //Extract multiple strings in a line
vector<string> extls(vector<string>&, int, int, string); //Extract multiple strings in a line

string rmspaces(string);
vector<string> rmblanklines(vector<string>&);
vector<string> rmcomments(vector<string>&);

void save_dat(const nns_project&, int);
void save_dat(const nns_project&,string, int);
void save_as_dat(const nns_project&,string,int);

#endif