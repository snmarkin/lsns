#include "networks.h"
#include "customfuncs.h"
#include "commands.h"
#include "datafiles.h"

nns_net& nns_net::operator= (const nns_net& net) {

	pconn = net.pconn;

	return *this;
};

void nns_net::init(vector<neural_population>* in_ppops) {
	name = "";
	OnOff = 0;
	pnameList.clear();
	pidList.clear();
	ppops = in_ppops;
	/*pconn.spop = -1;
	pconn.tpop = -1;
	pconn.sprob.val = 0;
	pconn.sprob.var = 0;
	pconn.tprob.val = 0;
	pconn.tprob.var = 0;
	pconn.w.val = 0;
	pconn.w.var = 0;*/
};

void nns_mnet::init(vector<neural_population>* in_ppops,vector<nns_net>* in_pnets) {
	name = "";
	OnOff = 0;
	nnameList.clear();
	nidList.clear();
	ppops = in_ppops;
	pnets = in_pnets;
	/*nconn.spop = -1;
	nconn.tpop = -1;
	nconn.sprob.val = 0;
	nconn.sprob.var = 0;
	nconn.tprob.val = 0;
	nconn.tprob.var = 0;
	nconn.w.val = 0;
	nconn.w.var = 0;*/
};

void nns_net::make_net(string in_name,vector<string>& popnameList,vector<int>& popidList, vector<string>& aunameList, vector<int>& auidList, vector<neural_population>* ppops) {
	
	//vector<int> idList;
	//int nnl = nameList.size();
	//for (int i = 0;i < nnl;i++) {
	//	idList.push_back(find_id(*ppops, nameList[i]));
	//};
	init(ppops);

	name = in_name;
	OnOff = 1;
	pnameList = popnameList;
	pidList = popidList;
	unameList = aunameList;
	uidList = auidList;

	npList = (int)popidList.size();
	nuList = (int)uidList.size();
	nList = npList + nuList;

};

nns_mnet& nns_mnet::operator= (const nns_mnet& mnet) {

	nconn = mnet.nconn;

	return *this;
};

void nns_mnet::make_mnet(string in_name,vector<string>& nameList,vector<neural_population>* ppops, vector<nns_net>* pnets) {
	vector<int> idList;
	for (size_t i = 0;i < nameList.size();i++) {
		idList.push_back(find_id(*pnets, nameList[i]));
	};
	init(ppops,pnets);

	name = in_name;
	OnOff = 1;
	nnameList = nameList;
	nidList = idList;
	nList = (int)idList.size();

};

void nns_net::conn_pops(int src,int srctype, param sprob,int trg,int trgtype, param tprob,param w, int syntype, int opt,bool optmz_OnOff,bool norm_OnOff) {
	//Create a new connection
	net_pconn tmp;
	//tmp = new net_pconn;
	pconn.push_back(tmp);
	
	//Assigning values
	int n = (int)pconn.size();
	pconn[n - 1].opt = opt;
	pconn[n-1].sid = src; //Now it is absolute pop id
	pconn[n - 1].stype = srctype;
	pconn[n-1].sprob = sprob;
	pconn[n-1].tid = trg; //Now it is absolute pop id
	pconn[n - 1].ttype = trgtype;
	pconn[n-1].tprob = tprob;
	pconn[n-1].w = w;
	pconn[n - 1].syntype = syntype;
	pconn[n - 1].optmz_OnOff = optmz_OnOff;
	pconn[n - 1].norm_OnOff = norm_OnOff;

	pconn[n - 1].counter = 0;

};

void nns_net::make_mtx(int id,contr_parameters& ctr_para) {
	param sprob = pconn[id].sprob;
	param tprob = pconn[id].tprob;
	param w = pconn[id].w;
	int src = pconn[id].sid;
	int trg = pconn[id].tid;
	int stype = pconn[id].stype;
	int ttype = pconn[id].ttype;
	int td;
	int tn;
	int *list;
	double *wlist;
	
	//Random generator setting
	if (ctr_para.srand_status != 1) {
		srand(ctr_para.seed);
		ctr_para.srand_status = 1;
	};
	
	RNG rng;

	switch (stype) {
	case UTYPE_POP:
		pconn[id].spmtx = new int[ppops->at(pconn[id].sid).nneurons]();
		tn = (int)(ppops->at(src).nneurons * sprob.val / 100. + 0.5f);
		td = int(round1((tn - tn*sprob.var / 100.) + (rand() / RAND_MAX) * 2 * tn*sprob.var / 100) + 0.5f);
		//td = int(round1((tn - tn*sprob.var / 100.) + (rand() / RAND_MAX) * 2 * tn*sprob.var / 100));
		list = new int[td];
		wlist = new double[td];
		list = rdipermarray(td, ppops->at(src).nneurons);
		for (int i = 0;i<td;i++) {
			pconn[id].spmtx[list[i]] = 1;
		}	
		if (w.var != 0) {
			pconn[id].wmtx = new double[ppops->at(pconn[id].sid).nneurons]();
			rng.seed(( unsigned long )time(0));
			wlist = normrdarray(rng, td, 2, w.val, w.var);
			for (int i = 0;i<td;i++) {
				pconn[id].wmtx[list[i]] = wlist[i];
			}
			pconn[id].nwmtx = ppops->at(pconn[id].sid).nneurons;
		}
		else {
			pconn[id].wmtx = new double[1];
			pconn[id].nwmtx = 1;
		}
		delete[] list;
		delete[] wlist;
		break;
	case UTYPE_NSU:
		pconn[id].spmtx = new int[1];//		pconn[id].wmtx = new double[1];
		
		//Added on 4.7.2016 =============== start
		if (w.var != 0) {
			switch (ttype) {
			case UTYPE_POP:
				tn = (int)(ppops->at(trg).nneurons * tprob.val / 100. + 0.5f);
				td = int(round1((tn - tn*tprob.var / 100.) + (rand() / RAND_MAX) * 2 * tn*tprob.var / 100) + 0.5f);
				list = new int[td];
				wlist = new double[td];
				list = rdipermarray(td, ppops->at(trg).nneurons);
				pconn[id].wmtx = new double[ppops->at(pconn[id].tid).nneurons]();
				rng.seed(( unsigned long)time(0));
				wlist = normrdarray(rng, td, 2, w.val, w.var);
				for (int i = 0;i<td;i++) {
					pconn[id].wmtx[list[i]] = wlist[i];
				}
				pconn[id].nwmtx = ppops->at(pconn[id].tid).nneurons;
				delete[] list;
				delete[] wlist;
				break;
			case UTYPE_NSU:
				pconn[id].wmtx = new double[1];
				pconn[id].nwmtx = 1;
				break;
			};
		}
		else {
			pconn[id].wmtx = new double[1];
		};
		//Added on 4.7.2016 =============== end
		
		break;
	};

	switch (ttype) {
	case UTYPE_POP:
		pconn[id].tpmtx = new int[ppops->at(pconn[id].tid).nneurons]();
		tn = (int)(ppops->at(trg).nneurons * tprob.val / 100. + 0.5f);
		td = int(round1((tn - tn*tprob.var / 100.) + (rand() / RAND_MAX) * 2 * tn*tprob.var / 100) + 0.5f);
		list = new int[td];
		list = rdipermarray(td, ppops->at(trg).nneurons);
		for (int i = 0;i<td;i++) {
			pconn[id].tpmtx[list[i]] = 1;
		}
		delete[] list;
		break;
	case UTYPE_NSU:
		pconn[id].tpmtx = new int[1];
		break;
	};

};

void nns_net::del_mtx(int id) {
	delete[] pconn[id].spmtx;
	delete[] pconn[id].tpmtx;
	delete[] pconn[id].wmtx;
};

void nns_net::conn_pops(int src, int srctype,int trg, int trgtype, int syntype, int opt, param w, int prate, bool optmz_OnOff, bool norm_OnOff) {

	//Create a new connection
	net_pconn tmp;
	//tmp = new net_pconn;
	pconn.push_back(tmp);

	//Assigning values
	int n = (int)pconn.size();
	pconn[n - 1].sid = src; //Now it is absolute pop id
	pconn[n - 1].stype = srctype;

	pconn[n - 1].tid = trg; //Now it is absolute pop id
	pconn[n - 1].ttype = trgtype;

	pconn[n - 1].syntype = syntype;
	pconn[n - 1].opt = opt;
	pconn[n - 1].prate = prate;
	pconn[n - 1].w = w;
	pconn[n - 1].optmz_OnOff = optmz_OnOff;
	pconn[n - 1].norm_OnOff = norm_OnOff;

	//Added on 4.7.2015
	pconn[n - 1].sprob.val = 100;
	pconn[n - 1].sprob.var = 0;
	pconn[n - 1].tprob.val = 100;
	pconn[n - 1].tprob.var = 0;
	pconn[n - 1].counter = 0;

};

void nns_net::del_conn(int src,int trg) {
	int n = (int)pconn.size();
	bool found = 0;
	for (int i=0;i<n;i++) {
		if (pconn[i].sid == src && pconn[i].tid == trg) {
			pconn.erase(pconn.begin() + i);
			found = 1;
			break;
		};
	};
	if (found != 1) 
		cout<<"Connection from pops("<<src<<") "<<ppops->at(src).name<<" to pops("<<trg<<") "<<ppops->at(trg).name<<": NOT found"<<endl;
	else{
		cout<<"Connection from pops("<<src<<") "<<ppops->at(src).name<<" to pops("<<trg<<") "<<ppops->at(trg).name<<": deleted"<<endl;
	};
};

void nns_mnet::conn_nets(int srcn,int srcp,param sprob,int trgn, int trgp,param tprob,param w, int type) {
	int mtxflg = 0;
	//Create a new connection
	net_nconn tmp;
	//tmp = new net_nconn;
	nconn.push_back(tmp);
	
	//Assigning values
	int n = (int)nconn.size();
	nconn[n-1].snet = srcn;
	nconn[n-1].sid = srcp; //Absolute pop id
	nconn[n-1].sprob = sprob;
	nconn[n-1].tnet = trgn;
	nconn[n-1].tid = trgp; //Absolute pop id
	nconn[n-1].tprob = tprob;
	nconn[n-1].w = w;
	nconn[n - 1].syntype = type;

	//Initializing matrixes
	nconn[n-1].spmtx = new int [ppops->at(srcp).nneurons];
	nconn[n-1].tpmtx = new int [ppops->at(trgp).nneurons];
	nconn[n-1].wmtx = new double [ppops->at(srcp).nneurons];

	//if (mtxflg == 1) {
	//	//Random generator setting
	//	if (ppops->at(srcp).paraset.ctr_para.srand_status != 1) {
	//		srand(ppops->at(srcp).paraset.ctr_para.seed);
	//		ppops->at(srcp).paraset.ctr_para.srand_status = 1;
	//	};
	//	RNG rng;

	//	//Assigning matrix values
	//	double tn = ppops->at(srcp).nneurons * sprob.val / 100.;
	//	int td = int(round1((tn - tn*sprob.var / 100.) + (rand() / RAND_MAX) * 2 * tn*sprob.var / 100));
	//	nconn[n - 1].spmtx = rdipermarray(td, ppops->at(srcp).nneurons);
	//	rng.seed(ppops->at(srcp).paraset.ctr_para.seed + rand());
	//	nconn[n - 1].wmtx = normrdarray(rng, ppops->at(srcp).nneurons, 2, w.val, w.var);

	//	tn = ppops->at(trgp).nneurons * tprob.val / 100.;
	//	td = int(round1((tn - tn*tprob.var / 100.) + (rand() / RAND_MAX) * 2 * tn*tprob.var / 100));
	//	nconn[n - 1].spmtx = rdipermarray(td, ppops->at(trgp).nneurons);

	//};

};

void nns_mnet::del_conn(int srcn,int trgn) {
	int n = (int)nconn.size();
	bool found = 0;
	for (int i=0;i<n;i++) {
		if (nconn[i].snet == srcn && nconn[i].tnet == trgn) {
			nconn.erase(nconn.begin() + i);
			found = 1;
			break;
		};
	};
	if (found != 1) 
		cout<<"Connection from nets("<<nidList[srcn]<<") "<<pnets->at(nidList[srcn]).name<<" to nets("<<nidList[trgn]<<") "<<pnets->at(nidList[trgn]).name<<": NOT found"<<endl;
	else{
		cout<<"Connection from nets("<<nidList[srcn]<<") "<<pnets->at(nidList[srcn]).name<<" to nets("<<nidList[trgn]<<") "<<pnets->at(nidList[trgn]).name<<": deleted"<<endl;
	};
};

void nns_tnet::reset() {
	OnOff = 0;
	nets.clear();
	mnets.clear();
};

void fix_ids(vector<nns_net>& nets, int cpy, int org) {
	size_t isrc = 0, itrg = 0; // SM fixed (initialization)
	for (size_t ipc = 0;ipc < nets[org].pconn.size();ipc++) {
		int src = nets[org].pconn[ipc].sid;
		int trg = nets[org].pconn[ipc].tid;
		for (size_t ipnl = 0;ipnl < nets[org].pidList.size();ipnl++) {
			if (nets[org].pidList[ipnl] == src) isrc = ipnl;
			if (nets[org].pidList[ipnl] == trg) itrg = ipnl;
		};
		nets[cpy].pconn[ipc].sid = nets[cpy].pidList[isrc];
		nets[cpy].pconn[ipc].tid = nets[cpy].pidList[itrg];
	};
}

void fix_ids(vector<nns_mnet>& mnets, int cpy, int org) {
	size_t srcn = 0, trgn = 0, isrcn = 0, itrgn = 0, srcp = 0, trgp = 0, isrcp = 0, itrgp = 0; //SM fixed (initialization)
	for (size_t ipc = 0;ipc < mnets[org].nconn.size();ipc++) {
		srcn = mnets[org].nconn[ipc].snet;
		trgn = mnets[org].nconn[ipc].tnet;
		for (size_t innl = 0;innl < mnets[org].nidList.size();innl++) {
			if (mnets[org].nidList[innl] == srcn) isrcn = innl;
			if (mnets[org].nidList[innl] == trgn) itrgn = innl;
		};
		mnets[cpy].nconn[ipc].snet = mnets[cpy].nidList[isrcn];
		mnets[cpy].nconn[ipc].tnet = mnets[cpy].nidList[itrgn];
		srcp = mnets[org].nconn[ipc].sid;
		trgp = mnets[org].nconn[ipc].tid;
		for (size_t is = 0;is < mnets[org].pnets->at(srcn).pidList.size();is++) {
			if (mnets[org].pnets->at(srcn).pidList[is] == srcp) isrcp = is;
		};
		for (size_t it = 0;it < mnets[org].pnets->at(trgn).pidList.size();it++) {
			if (mnets[org].pnets->at(trgn).pidList[it] == trgp) itrgp = it;
		};
		mnets[cpy].nconn[ipc].sid = mnets[cpy].pnets->at(mnets[cpy].nconn[ipc].snet).pidList[isrcp];
		mnets[cpy].nconn[ipc].tid = mnets[cpy].pnets->at(mnets[cpy].nconn[ipc].tnet).pidList[itrgp];
	};
}


void make_net(string name, vector<string>& plist, vector<string>& ulist, vector<neural_population>& pops, nns_nonspikeunit& nsu, vector<nns_net>& nets) {

	size_t nn = nets.size();
	vector<int> pidList;
	vector<int> uidList;

	//Check if population names in the list exist in neural populations
	int pc;
	for (size_t i = 0;i<plist.size();i++) {
		pc = 0;
		for (size_t j = 0;j<pops.size();j++) {
			if (plist[i] == pops[j].name) {
				pidList.push_back((int)j);
				pc++;
				break;
			};
		};
		if (pc == 0) {
			cout << "ERROR:: Population " << plist[i] << ": NOT exist in the population list" << endl;
			return;
		};
	};

	//Check if nonspike unit names in the list exist in nonspike units
	int uc;
	for (size_t i = 0;i<ulist.size();i++) {
		uc = 0;
		string tmp = ulist[i];
		int utype = getrmtype(tmp);
		for (size_t j = 0;j<nsu.units.size();j++) {
			if (tmp == nsu.units[j].name) {
				uidList.push_back((int)j);
				uc++;
				break;
			};
		};
	
		if (uc == 0) {
			cout << "ERROR::NSUNIT [" << ulist[i] << "]: NOT exist in the nonspike unit list" << endl;
			return;
		};
	};

	size_t sid, sid2;
	stringstream stmp;
	//Check if the same network name exists in networks
	//If exists, add _c and a number to the end of name
	if (nn != 0) {
		sid = name.find("_c");
		if (sid != std::string::npos) {
			name = name.substr(0, sid);
		};
		pc = 0;
		for (size_t i = 0;i<nn;i++) {
			sid = nets[i].name.find(name);
			if (sid != std::string::npos) {
				sid2 = nets[i].name.find("_c");
				if ((nets[i].name == name) || (sid2 != std::string::npos))
					pc++;
			};
		};
		if (pc != 0) {
			stmp << name << "_c" << (pc - 1);
			name = stmp.str();
		};
	};

	nns_net tmp;
	nets.push_back(tmp);

	nets[nn].make_net(name, plist,pidList,ulist,uidList, &pops);

};

void conn_pops(string netname, int syntype, int opt, string sname, string tname, int stype, int ttype, param sprb, param tprb, param w, bool optmz_OnOff, bool norm_OnOff, nns_nonspikeunit& nsu, vector<nns_net>& nets) {
	int nid = find_id(nets, netname);
	string tmpstr;

	if (nid == -1) {
		cout << "ERROR::NET [" << netname << "]: NOT found in the network list" << endl;
		return;
	};
	int sid = -1, tid = -1;
	switch (stype) {
	case UTYPE_POP:
		//find the source population name and its id in the network
		for (int i = 0;i < nets[nid].npList;i++) {
			if (nets[nid].pnameList[i] == sname) {
				sid = nets[nid].pidList[i];
				break;
			};
		};
		break;
	case UTYPE_NSU:
		//find the source population name and its id in the network
		for (int i = 0;i < nets[nid].nuList;i++) {
			if (nets[nid].unameList[i] == sname) {
				sid = nets[nid].uidList[i];
				break;
			};
		};
		break;
	};
	switch (ttype) {
	case UTYPE_POP:
		//find the source population name and its id in the network
		for (int i = 0;i < nets[nid].npList;i++) {
			if (nets[nid].pnameList[i] == tname) {
				tid = nets[nid].pidList[i];
				break;
			};
		};
		break;
	case UTYPE_NSU:
		//find the source population name and its id in the network
		for (int i = 0;i < nets[nid].nuList;i++) {
			if (nets[nid].unameList[i] == tname) {
				tid = nets[nid].uidList[i];
				break;
			};
		};
		break;
	};
	if (sid == -1) {
		switch (stype) {
		case UTYPE_POP:
			cout << "ERROR::POP [" << sname << "]: NOT found on the population name list of net [" << netname << "]" << endl;
			return;
		case UTYPE_NSU:
			cout << "ERROR::NSUNIT [" << sname << "]: NOT found on the nsu name list of net [" << netname << "]" << endl;
			return;
		};
	};
	if (tid == -1) {
		switch (ttype) {
		case UTYPE_POP:
			cout << "ERROR::POP [" << tname << "]: NOT found on the population name list of  net [" << netname << "]" << endl;
			return;
		case UTYPE_NSU:
			cout << "ERROR::NSUNIT [" << tname << "]: NOT found on the nsu name list of  net [" << netname << "]" << endl;
			return;
		};
	};

	//if (nsu.units[sid].type == NSU_OUTPUT) {
	//	cout << "ERROR::CONN Output [" << sname << "]: Cannot be a source" << endl;
	//	return;
	//};
	if (ttype == UTYPE_NSU) {
		if (nsu.units[tid].type == NSU_DRIVE) {
			cout << "ERROR::CONN Drive [" << tname << "]: Cannot be a target" << endl;
			return;
		};
	};

	nets[nid].conn_pops(sid, stype, sprb, tid, ttype, tprb, w, syntype, opt, optmz_OnOff, norm_OnOff);
};

void conn_pops(string netname, int syntype, int opt, string sname, string tname, int stype, int ttype, param w, int prate, bool optmz_OnOff, bool norm_OnOff, nns_nonspikeunit& nsu, vector<nns_net>& nets) {
	int nid = find_id(nets, netname);
	string tmpstr;

	if (nid == -1) {
		cout << "ERROR::" << netname << ": NOT found in the network list" << endl;
		return;
	};

	int sid = -1, tid = -1;
	switch (stype) {
	case UTYPE_POP:
		//find the source population name and its id in the network
		for (int i = 0;i < nets[nid].npList;i++) {
			if (nets[nid].pnameList[i] == sname) {
				sid = nets[nid].pidList[i];
				break;
			};
		};
		break;
	case UTYPE_NSU:
		//find the source unit name and its id in the network
		for (int i = 0;i < nets[nid].nuList;i++) {
			if (nets[nid].unameList[i] == sname) {
				sid = nets[nid].uidList[i];
				break;
			};
		};
		break;
	};
	switch (ttype) {
	case UTYPE_POP:
		//find the target population name and its id in the network
		for (int i = 0;i < nets[nid].npList;i++) {
			if (nets[nid].pnameList[i] == tname) {
				tid = nets[nid].pidList[i];
				break;
			};
		};
		break;
	case UTYPE_NSU:
		//find the target unit name and its id in the network
		for (int i = 0;i < nets[nid].nuList;i++) {
			if (nets[nid].unameList[i] == tname) {
				tid = nets[nid].uidList[i];
				break;
			};
		};
		break;
	};
	if (sid == -1) {
		switch (stype) {
		case UTYPE_POP:
			cout << "ERROR::POP [" << sname << "]: NOT found on the population name list of net [" << netname << "]" << endl;
			return;
		case UTYPE_NSU:
			cout << "ERROR::NSUNIT [" << sname << "]: NOT found on the drive name list of net [" << netname << "]" << endl;
			return;
		};
	};
	if (tid == -1) {
		switch (ttype) {
		case UTYPE_POP:
			cout << "ERROR::POP [" << tname << "]: NOT found on the population name list of  net [" << netname << "]" << endl;
			return;
		case UTYPE_NSU:
			cout << "ERROR::NSUNIT [" << tname << "]: NOT found on the output name list of  net [" << netname << "]" << endl;
			return;
		};
	};

	//if (nsu.units[sid].type == NSU_OUTPUT) {
	//	cout << "ERROR::CONN Output [" << sname << "]: Cannot be a source" << endl;
	//	return;
	//};
	if (ttype == UTYPE_NSU) {
		if (nsu.units[tid].type == NSU_DRIVE) {
			cout << "ERROR::CONN Drive [" << tname << "]: Cannot be a target" << endl;
			return;
		};
	};

	nets[nid].conn_pops(sid, stype, tid, ttype, syntype, opt, w, prate, optmz_OnOff, norm_OnOff);
};

void copy_net(string netname, vector<neural_population>& pops, nns_nonspikeunit& nsu, vector<nns_net>& nets) {

	size_t nn = nets.size();
	size_t np = pops.size();

	int nid = find_id(nets, netname);

	if (nid == -1) {
		cout << "ERROR::" << netname << ": NOT found in networks" << endl;
		return;
	};

	int npl = nets[nid].npList;
	int nul = nets[nid].nuList;
	vector<string> pnameList;
	vector<string> unameList;

	for (int i = 0;i<npl;i++) {
		copy_pop(pops, nets[nid].pnameList[i]);
		pnameList.push_back(pops[np + i].name);
	};
	for (int i = 0;i<npl;i++) {
		copy_pop(pops, nets[nid].unameList[i]);
		unameList.push_back(pops[np + i].name);
	};

	make_net(netname, pnameList, unameList, pops, nsu, nets);
	nets[nn] = nets[nid];
	fix_ids(nets, (int)nn, nid); //This is to fix wrong absolute ids by simple copying function

};

void rename_net(string oldname, string newname, vector<nns_net>& nets) {
	int nn = (int)nets.size();
	int nid = find_id(nets, oldname);

	if (nid != -1) {
		int tid = find_id(nets, newname);
		if (tid == -1) {
			nets[nid].name = newname;
			cout << "NET [" << oldname << "]: renamed as [" << newname << "]" << endl;
		}
		else {
			cout << "ERROR::NET [" << newname << "]: exists in the network list" << endl;
		};
	}
	else {
		cout << "ERROR::NET [" << oldname << "]: Not found in the network list" << endl;
	};
};

void delete_net(string name, vector<nns_net>& nets) {
	int nn = (int)nets.size();
	int found = -1;
	for (int i = 0;i<nn;i++) {
		if (nets[i].name == name) {
			nets.erase(nets.begin() + i);
			found = i;
			break;
		};
	};
	if (found != -1)
		cout << "NET [" << name << "]: deleted from networks" << endl;
	else
		cout << "ERROR::NET [" << name << "]: NOT found in networks" << endl;
};

void make_mnet(string name, vector<string>& list, vector<neural_population>& pops, vector<nns_net>& nets, vector<nns_mnet>& mnets) {

	size_t nm = mnets.size();
	vector<int> idList;

	//Check if network names in the list exist in neural networks
	int c;
	for (size_t i = 0;i<list.size();i++) {
		c = 0;
		for (size_t j = 0;j<nets.size();j++) {
			if (list[i] == nets[j].name) {
				idList.push_back((int)j);
				c++;
				break;
			};
		};
		if (c == 0) {
			cout << "ERROR::NET [" << list[i] << "]: NOT exist in the network list" << endl;
			return;
		};
	};

	size_t sid, sid2;
	stringstream stmp;
	//Check if the same macro network name exists in the macro network list
	//If exists, add _c and a number to the end of name
	if (nm != 0) {
		sid = name.find("_c");
		if (sid != std::string::npos) {
			name = name.substr(0, sid);
		};
		c = 0;
		for (size_t i = 0;i<nm;i++) {
			sid = mnets[i].name.find(name);
			if (sid != std::string::npos) {
				sid2 = mnets[i].name.find("_c");
				if ((mnets[i].name == name) || (sid2 != std::string::npos))
					c++;
			};
		};
		if (c != 0) {
			stmp << name << "_c" << (c - 1);
			name = stmp.str();
		};
	};

	nns_mnet *tmp = new nns_mnet;
	mnets.push_back(*tmp);

	mnets[nm].make_mnet(name, list, &pops, &nets);
};

void conn_nets(string mnetname, int type, string snname, string tnname, string spname, string tpname, param sprb, param tprb, param w, vector<nns_net>& nets, vector<nns_mnet>& mnets) {
	int nm = (int)mnets.size();
	int mnid = find_id(mnets, mnetname);

	if (mnid == -1) {
		cout << "ERROR::MNET [" << mnetname << "]: NOT found in the network list" << endl;
		return;
	};

	int snid = -1, tnid = -1;
	//find the source network name and its id in the macro network
	for (int i = 0;i<mnets[mnid].nList;i++) {
		if (mnets[mnid].nnameList[i] == snname) {
			snid = mnets[mnid].nidList[i];
		};
		if (mnets[mnid].nnameList[i] == tnname) {
			tnid = mnets[mnid].nidList[i];
		};
	};
	if (snid == -1) {
		cout << "ERROR::" << snname << ": NOT found on the network name list of mnet (" << mnetname << ")" << endl;
		return;
	};
	if (tnid == -1) {
		cout << "ERROR::" << tnname << ": NOT found on the network name list of mnet (" << mnetname << ")" << endl;
		return;
	};

	int spid = -1, tpid = -1;
	//find the source population name and its id in the network
	for (int i = 0;i<nets[snid].nList;i++) {
		if (nets[snid].pnameList[i] == spname) {
			spid = nets[snid].pidList[i];
		};
	};
	//find the target population name and its id in the network
	for (int i = 0;i<nets[tnid].nList;i++) {
		if (nets[tnid].pnameList[i] == tpname) {
			tpid = nets[tnid].pidList[i];
		};
	};
	if (spid == -1) {
		cout << "ERROR::" << spname << ": NOT found on the population name list of net (" << nets[mnets[mnid].nidList[snid]].name << ")" << endl;
		return;
	};
	if (tpid == -1) {
		cout << "ERROR::" << tpname << ": NOT found on the population name list of net (" << nets[mnets[mnid].nidList[tnid]].name << ")" << endl;
		return;
	};

	mnets[mnid].conn_nets(snid, spid, sprb, tnid, tpid, tprb, w, type);
};

void copy_mnet(string mnetname, vector<neural_population>& pops, nns_nonspikeunit& nsu, vector<nns_net>& nets, vector<nns_mnet>& mnets) {

	int nm = (int)mnets.size();
	int nn = (int)nets.size();

	int nid = find_id(mnets, mnetname);

	if (nid == -1) {
		cout << "ERROR::" << mnetname << ": NOT found in the macro network list" << endl;
		return;
	};

	int nl = mnets[nid].nList;
	vector<string> nameList;

	//Making new networks with previous network info.
	for (int i = 0;i<nl;i++) {
		copy_net(mnets[nid].nnameList[i], pops, nsu, nets);
		nameList.push_back(nets[nn + i].name);
	};

	make_mnet(mnetname, nameList, pops, nets, mnets);
	mnets[nm] = mnets[nid];
	fix_ids(mnets, nm, nid);

};

void rename_mnet(string oldname, string newname, vector<nns_mnet>& mnets) {
	int nm = (int)mnets.size();
	int nid = find_id(mnets, oldname);
	if (nid != -1) {
		int tid = find_id(mnets, newname);
		if (tid == -1) {
			mnets[nid].name = newname;
			cout << "MNET [" << oldname << "]: renamed as [" << newname << "]" << endl;
		}
		else {
			cout << "ERROR::MNET [" << newname << "]: already exists in the macro network list" << endl;
		};
	}
	else {
		cout << "ERROR::MNET [" << oldname << "]: Not found in the macro network list" << endl;
	};
};

void delete_mnet(string name, vector<nns_mnet>& mnets) {
	int nm = (int)mnets.size();
	int found = -1;
	for (int i = 0;i<nm;i++) {
		if (mnets[i].name == name) {
			mnets.erase(mnets.begin() + i);
			found = i;
			break;
		};
	};
	if (found != -1)
		cout << "MNET [" << name << "]: deleted from the macro network list" << endl;
	else
		cout << "ERROR::MNET [" << name << "]: NOT found in the macro network list" << endl;
};
