#include "ichannels.h"
#include "cal_macro_funcs.h"

General_IChannel::General_IChannel() {
	name = "";
	ID = 0;
	I = 0;
	G = 0;
	M = 0;
	H = 0;
	m_inf = 0;
	h_inf = 0;
	Tm = 0;
	Th = 0;
	Vc = 0;
};

Act_IChannel::Act_IChannel() {
	name = "";
	ID = 0;
	I = 0;
	G = 0;
	M = 0;
	m_inf = 0;
	Tm = 0;
	Vc = 0;
};

InAct_IChannel::InAct_IChannel() {
	name = "";
	ID = 0;
	I = 0;
	G = 0;
	H = 0;
	h_inf = 0;
	Th = 0;
	Vc = 0;
};

NoGate_IChannel::NoGate_IChannel() {
	name = "";
	ID = 0;
	I = 0;
	G = 0;
	g_max = 0;
	Vc = 0;
};

General_IChannel::~General_IChannel() {
	//delete name;
	//delete &ID;
	//delete &I;
	//delete &G;
	//delete &M;		//Activation of voltage-gated channel
	//delete &H;		//Inactivation of voltage-gated channel
	//delete &m_inf;	//steady-state activation
	//delete &h_inf;	//steady-state inactivation
	//delete &Tm;		//Voltage dependent time constant for activation	
	//delete &Th;		//Voltage dependent time constant for inactivation
	//delete &Vc;		//Current Voltage
};

Act_IChannel::~Act_IChannel() {
	//delete name;
	//delete &ID;
	//delete &I;
	//delete &G;
	//delete &M;		//Activation of voltage-gated channel
	//delete &m_inf;	//steady-state activation
	//delete &Tm;		//Voltage dependent time constant for activation	
	//delete &Vc;		//Current Voltage

};

InAct_IChannel::~InAct_IChannel() {
	//delete name;
	//delete &ID;
	//delete &I;
	//delete &G;
	//delete &H;		//Activation of voltage-gated channel
	//delete &h_inf;	//steady-state activation
	//delete &Th;		//Voltage dependent time constant for activation	
	//delete &Vc;		//Current Voltage

};

NoGate_IChannel::~NoGate_IChannel() {
	//delete name;
	//delete &ID;
	//delete & I;		//Current
	//delete & G;		//Conductance
	//delete & g_max;	//Maximum Conductance
	//delete & Vc;		//Current Voltage
};

int General_IChannel::init_ICh(strGeneral_IChannel &GenChParam) {

	name = GenChParam.name;
	ID = GenChParam.ID;
	G = GenChParam.G;

	Vc = GenChParam.Vc;

	return 0;
};

General_IChannel& General_IChannel::operator= (const General_IChannel& channel){
	name = channel.name;
	G = channel.G;
	I = channel.I;
	M = channel.M;
	H = channel.H;
	return *this;
};

void General_IChannel::copy_ICh(strGeneral_IChannel& GenChParam) {

	GenChParam.name = name;
	GenChParam.ID = ID;
	GenChParam.Vc = Vc;

};


void General_IChannel::cal_all(double ODEstep,const vector<strCom_Gen1Para>& com1p,const vector<strCom_GenPara>& comp,int tp,int id) {
	double tmp1, tmp2, tmp3;
	tmp1 = (comp[tp].Vhfm.size() == 1)? comp[tp].Vhfm[0]:comp[tp].Vhfm[id];
	tmp2 = (comp[tp].tm_max.size() == 1)? comp[tp].tm_max[0]:comp[tp].tm_max[id];
	tmp3 = (comp[tp].ktm.size() == 1)? comp[tp].ktm[0]:comp[tp].ktm[id];
	Tm = CAL_TM(Vc,tmp1,tmp2,tmp3);

	tmp2 = (comp[tp].km.size() == 1)? comp[tp].km[0]:comp[tp].km[id];
	m_inf = CAL_M_INF(Vc,tmp1,tmp2);
	M = ODE_EULER(M,m_inf,ODEstep,Tm);

	tmp1 = (comp[tp].Vhfh.size() == 1)? comp[tp].Vhfh[0]:comp[tp].Vhfh[id];
	tmp2 = (comp[tp].th_max.size() == 1)? comp[tp].th_max[0]:comp[tp].th_max[id];
	tmp3 = (comp[tp].kth.size() == 1)? comp[tp].kth[0]:comp[tp].kth[id];
	Th = CAL_TM(Vc,tmp1,tmp2,tmp3);

	tmp2 = (comp[tp].kh.size() == 1)? comp[tp].kh[0]:comp[tp].kh[id];
	h_inf = CAL_H_INF(Vc,tmp1,tmp2);
	H = ODE_EULER(H,h_inf,ODEstep,Th);
	tmp1=1;
	for (int i=0;i<com1p[tp].mpower; i++) tmp1 *= M;
	tmp2=1;
	for (int i=0;i<com1p[tp].hpower; i++) tmp2 *= H;

	I = CAL_I(G,tmp1,tmp2,Vc,(comp[tp].E.size() == 1) ? comp[tp].E[0]:comp[tp].E[id]);
	
};

void General_IChannel::cal_all(double V, double ODEstep,const vector<strCom_Gen1Para>& com1p,const vector<strCom_GenPara>& comp,int tp,int id) {
	Vc = V;
	double tmp1, tmp2, tmp3;
	tmp1 = (comp[tp].Vhfm.size() == 1)? comp[tp].Vhfm[0]:comp[tp].Vhfm[id];
	tmp2 = (comp[tp].tm_max.size() == 1)? comp[tp].tm_max[0]:comp[tp].tm_max[id];
	tmp3 = (comp[tp].ktm.size() == 1)? comp[tp].ktm[0]:comp[tp].ktm[id];
	Tm = CAL_TM(Vc,tmp1,tmp2,tmp3);

	tmp2 = (comp[tp].km.size() == 1)? comp[tp].km[0]:comp[tp].km[id];
	m_inf = CAL_M_INF(Vc,tmp1,tmp2);
	M = ODE_EULER(M,m_inf,ODEstep,Tm);

	tmp1 = (comp[tp].Vhfh.size() == 1)? comp[tp].Vhfh[0]:comp[tp].Vhfh[id];
	tmp2 = (comp[tp].th_max.size() == 1)? comp[tp].th_max[0]:comp[tp].th_max[id];
	tmp3 = (comp[tp].kth.size() == 1)? comp[tp].kth[0]:comp[tp].kth[id];
	Th = CAL_TM(Vc,tmp1,tmp2,tmp3);

	tmp2 = (comp[tp].kh.size() == 1)? comp[tp].kh[0]:comp[tp].kh[id];
	h_inf = CAL_H_INF(Vc,tmp1,tmp2);
	H = ODE_EULER(H,h_inf,ODEstep,Th);
	tmp1=1;
	for (int i=0;i<com1p[tp].mpower; i++) tmp1 *= M;
	tmp2=1;
	for (int i=0;i<com1p[tp].hpower; i++) tmp2 *= H;
	
	I = CAL_I(G,tmp1,tmp2,Vc,(comp[tp].E.size() == 1) ? comp[tp].E[0]:comp[tp].E[id]);
	
};


int Act_IChannel::init_ICh(strAct_IChannel& GenChParam) {

	name = GenChParam.name;
	ID = GenChParam.ID;
	G = GenChParam.G;

	Vc = GenChParam.Vc;

	return 0;
};

int Act_IChannel::init_ICh(strGeneral_IChannel& GenChParam) {

	name = GenChParam.name;
	ID = GenChParam.ID;
	G = GenChParam.G;

	Vc = GenChParam.Vc;

	return 0;
};

void Act_IChannel::copy_ICh(strAct_IChannel& GenChParam) {

	GenChParam.name = name;
	GenChParam.ID = ID;
	GenChParam.G = G;

	GenChParam.Vc = Vc;

};

void Act_IChannel::cal_all(double ODEstep,const vector<strCom_Gen1Para>& com1p,const vector<strCom_GenPara>& comp,int tp,int id) {
	double tmp1, tmp2, tmp3;
	tmp1 = (comp[tp].Vhfm.size() == 1)? comp[tp].Vhfm[0]:comp[tp].Vhfm[id];
	tmp2 = (comp[tp].tm_max.size() == 1)? comp[tp].tm_max[0]:comp[tp].tm_max[id];
	tmp3 = (comp[tp].ktm.size() == 1)? comp[tp].ktm[0]:comp[tp].ktm[id];
	Tm = CAL_TM(Vc,tmp1,tmp2,tmp3);

	tmp2 = (comp[tp].km.size() == 1)? comp[tp].km[0]:comp[tp].km[id];
	m_inf = CAL_M_INF(Vc,tmp1,tmp2);
	M = ODE_EULER(M,m_inf,ODEstep,Tm);
	//M = CAL_M(m_inf,M,ODEstep,Tm);

	tmp1=1;
	for (int i=0;i<com1p[tp].mpower; i++) tmp1 *= M;

	I = CAL_IM(G,tmp1,Vc,(comp[tp].E.size() == 1) ? comp[tp].E[0]:comp[tp].E[id]);
};

void Act_IChannel::cal_all(double V,double ODEstep,const vector<strCom_Gen1Para>& com1p,const vector<strCom_GenPara>& comp,int tp,int id) {
	Vc = V;
	double tmp1, tmp2, tmp3;
	tmp1 = (comp[tp].Vhfm.size() == 1)? comp[tp].Vhfm[0]:comp[tp].Vhfm[id];
	tmp2 = (comp[tp].tm_max.size() == 1)? comp[tp].tm_max[0]:comp[tp].tm_max[id];
	tmp3 = (comp[tp].ktm.size() == 1)? comp[tp].ktm[0]:comp[tp].ktm[id];
	Tm = CAL_TM(Vc,tmp1,tmp2,tmp3);

	tmp2 = (comp[tp].km.size() == 1)? comp[tp].km[0]:comp[tp].km[id];
	m_inf = CAL_M_INF(Vc,tmp1,tmp2);
	M = ODE_EULER(M,m_inf,ODEstep,Tm);
	tmp1=1;
	for (int i=0;i<com1p[tp].mpower; i++) tmp1 *= M;


	I = CAL_IM(G,tmp1,Vc,(comp[tp].E.size() == 1) ? comp[tp].E[0]:comp[tp].E[id]);
};

int InAct_IChannel::init_ICh(strInAct_IChannel& GenChParam) {

	name = GenChParam.name;
	ID = GenChParam.ID;
	G = GenChParam.G;

	Vc = GenChParam.Vc;

	return 0;
};

int InAct_IChannel::init_ICh(strGeneral_IChannel& GenChParam) {

	name = GenChParam.name;
	ID = GenChParam.ID;
	G = GenChParam.G;

	Vc = GenChParam.Vc;

	return 0;
};


void InAct_IChannel::copy_ICh(strInAct_IChannel& GenChParam) {

	GenChParam.name = name;
	GenChParam.ID = ID;
	GenChParam.G = G;

	GenChParam.Vc = Vc;

};


void InAct_IChannel::cal_all(double ODEstep,const vector<strCom_Gen1Para>& com1p,const  vector<strCom_GenPara>& comp,int tp,int id) {
	double tmp1, tmp2, tmp3;

	tmp1 = (comp[tp].Vhfh.size() == 1)? comp[tp].Vhfh[0]:comp[tp].Vhfh[id];
	tmp2 = (comp[tp].th_max.size() == 1)? comp[tp].th_max[0]:comp[tp].th_max[id];
	tmp3 = (comp[tp].kth.size() == 1)? comp[tp].kth[0]:comp[tp].kth[id];
	Th = CAL_TM(Vc,tmp1,tmp2,tmp3);

	tmp2 = (comp[tp].kh.size() == 1)? comp[tp].kh[0]:comp[tp].kh[id];
	h_inf = CAL_H_INF(Vc,tmp1,tmp2);
	H = ODE_EULER(H,h_inf,ODEstep,Th);
	tmp1=1;
	for (int i=0;i<com1p[tp].hpower; i++) tmp1 *= H;

	I = CAL_IH(G,tmp1,Vc,(comp[tp].E.size() == 1) ? comp[tp].E[0]:comp[tp].E[id]);
};

void InAct_IChannel::cal_all(double V,double ODEstep,const vector<strCom_Gen1Para>& com1p,const  vector<strCom_GenPara>& comp,int tp,int id) {
	Vc = V;
	double tmp1, tmp2, tmp3;

	tmp1 = (comp[tp].Vhfh.size() == 1)? comp[tp].Vhfh[0]:comp[tp].Vhfh[id];
	tmp2 = (comp[tp].th_max.size() == 1)? comp[tp].th_max[0]:comp[tp].th_max[id];
	tmp3 = (comp[tp].kth.size() == 1)? comp[tp].kth[0]:comp[tp].kth[id];
	Th = CAL_TM(Vc,tmp1,tmp2,tmp3);

	tmp2 = (comp[tp].kh.size() == 1)? comp[tp].kh[0]:comp[tp].kh[id];
	h_inf = CAL_H_INF(Vc,tmp1,tmp2);
	H = ODE_EULER(H,h_inf,ODEstep,Th);
	tmp1=1;
	for (int i=0;i<com1p[tp].hpower; i++) tmp1 *= H;

	I = CAL_IH(G,tmp1,Vc,(comp[tp].E.size() == 1) ? comp[tp].E[0]:comp[tp].E[id]);
};

int NoGate_IChannel::init_ICh(strNoGate_IChannel& GenChParam) {

	name = GenChParam.name;
	ID = GenChParam.ID;
	I = GenChParam.I;
	G = GenChParam.G;
	Vc = GenChParam.Vc;

	return 0;
};

int NoGate_IChannel::init_ICh(strGeneral_IChannel& GenChParam) {

	name = GenChParam.name;
	ID = GenChParam.ID;
	I = GenChParam.I;
	G = GenChParam.G;
	Vc = GenChParam.Vc;

	return 0;
};


void NoGate_IChannel::copy_ICh(strNoGate_IChannel& GenChParam) {

	GenChParam.name = name;
	GenChParam.ID = ID;
	GenChParam.Vc = Vc;
	GenChParam.G = G;

};

void NoGate_IChannel::cal_all(const vector<strCom_GenPara>& comp,int tp,int id) {

	I = CAL_ING(G,Vc,(comp[tp].E.size() == 1) ? comp[tp].E[0]:comp[tp].E[id]);

};

void NoGate_IChannel::cal_all(double V, const vector<strCom_GenPara>& comp,int tp,int id) {
	Vc = V;
	I = CAL_ING(G,Vc,(comp[tp].E.size() == 1) ? comp[tp].E[0]:comp[tp].E[id]);

};


//void create_ich(vector<ich_m_parameters>& m_para,string name,int ID){
//	ich_m_parameters *ich;
//	ich = new ich_m_parameters;
//	ich->name = name;
//	ich->ID = ID;
//	m_para.push_back(*ich);
//
//};
//void create_ich(vector<ich_h_parameters>& h_para,string name,int ID){
//	ich_h_parameters *ich;
//	ich = new ich_h_parameters;
//	ich->name = name;
//	ich->ID = ID;
//	h_para.push_back(*ich);
//
//};
//void create_ich(vector<ich_ng_parameters>& ng_para,string name,int ID){
//	ich_ng_parameters *ich;
//	ich = new ich_ng_parameters;
//	ich->name = name;
//	ich->ID = ID;
//	ng_para.push_back(*ich);
//
//};
