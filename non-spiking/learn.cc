#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include "par.h"

using namespace std;

const int nc=100; // number of cues
const int na=100; // number of actions

inline float rnd() { return 1.*rand()/RAND_MAX; }

// activation function
inline float s(float x) { return x>0?tanh(x):0; }

// exploration distribution
float A_exp = 0.2; // exploratory constant [0, 1]

float y_drive=.6;
float y_d1=2, y_d2=2;
float d1_gpi=2, init_gpi_drive=2, gpi_y=1.5;
float gpi_drive = init_gpi_drive - A_exp; // modify gpi_drive
float d2_gpe=2, gpe_drive=2, gpe_gpi=1.5;
float tau=1; // time constant for Wilson-Cowan

float inh1=0; // lateral inhibition in D1
float inh2=0; // lateral inhibition in D2
float inh21=.0; // inhibition of D1 by D2
float inh12=.0; // inhibition of D2 by D1
float inhy=.6; // lateral inhibition in M1

float d1[na]={},d2[na]={};
float gpe[na],gpi[na];

void bg_step(float **w1,float **w2,float **wm,float *x,float *y,float dt,float* expl,float flag)
{
	float D1[na]={},D2[na]={},Y[na]={};
	for(int i=0;i<na;i++)
	{
		D1[i]=-inh21*d2[i];
		D2[i]=-inh12*d1[i];
		Y[i]=y_drive;
		for(int j=0;j<nc;j++)
		{
			D1[i]+=w1[j][i]*x[j];
			D2[i]+=w2[j][i]*x[j];
			Y[i]+=wm[j][i]*x[j];
		}
		for(int j=0;j<na;j++) if(i!=j) { D1[i]-=inh1*d1[j]; D2[i]-=inh2*d2[j]; Y[i]-=inhy*y[j]; }
		D1[i]=s(D1[i]+y_d1*y[i]);
		D2[i]=s(D2[i]+y_d2*y[i]);
	}
	for(int i=0;i<na;i++)
	{
		gpe[i]=s(-d2_gpe*d2[i]+gpe_drive);
		
		gpi[i]=flag*s(-d1_gpi*d1[i]-gpe_gpi*gpe[i]+gpi_drive+expl[i]);
		
		Y[i]=s(Y[i]-gpi_y*gpi[i]);
		
		y[i]+=(Y[i]-y[i])*(1-exp(-dt/tau));
		d1[i]+=(D1[i]-d1[i])*(1-exp(-dt/tau));
		d2[i]+=(D2[i]-d2[i])*(1-exp(-dt/tau));
	}
}

float gam1=.005;
float gam2=.005;

float lam1=.5; // learning rate
float lam2=.3; // learning rate

float mgam=.002;
float mlam=.002;

float W1max=3;
float W2max=3;

void bg_learn(float **w1,float **w2,float* x,float* y,float DA,float **wm)
{
	for(int i=0;i<na;i++)
	{
		for(int j=0;j<nc;j++)
		{
			w1[j][i]+= lam1*DA*x[j]*d1[i]-gam1*w1[j][i];
			w2[j][i]+=-lam2*DA*x[j]*d2[i]-gam2*w2[j][i];
			if(w1[j][i]<0) w1[j][i]=0;
			if(w2[j][i]<0) w2[j][i]=0;
			if(w1[j][i]>W1max) w1[j][i]=W1max;
			if(w2[j][i]>W2max) w2[j][i]=W2max;
		}
	}
	for(int i=0;i<na;i++) for(int j=0;j<nc;j++) wm[j][i]+=mlam*x[j]*y[i]-mgam*wm[j][i];
}



float gauss()
{
	static int flag=0;
 	static float x1, x2, w, y1, y2;

 	if(flag)
 	{
 		flag=0;
 		return y2;
 	}
 	flag=1;
 
    do 
    {
    	x1 = 2.0 * rnd() - 1.0;
        x2 = 2.0 * rnd() - 1.0;
        w = x1 * x1 + x2 * x2;
    } while ( w >= 1.0 );

    w = sqrt( (-2.0 * log( w ) ) / w );
    y1 = x1 * w;
    y2 = x2 * w;
    return y1;
}

inline float sgn(float x) { return x>0?1:-1; }

int main()
{
  srand(time(NULL));
	float *w1[nc],*w2[nc],*wm[nc];
	for(int j=0;j<nc;j++)
	{
		w1[j]=new float [na];
		w2[j]=new float [na];
		wm[j]=new float [na];
		for(int i=0;i<na;i++)
		{
			w1[j][i]=.01*rnd();
			w2[j][i]=.01*rnd();
			wm[j][i]=i==j?0:0;
		}
	}

	float x[nc]={},y[na]={};
	x[25]=1;
	float dt=.1,T=10;
	ofstream out("qqq");
	
	reach_init();
	float phi0[2]={};
	ifstream("ini")>>phi0[0]>>phi0[1];
	float xc=(-L1*sin(phi0[0])+-L2*sin(phi0[1])),yc=(L1*cos(phi0[0])+L2*cos(phi0[1]));

	for(int k=0;k<1000;k++)
	{
		static float Rpre=0;
		
		float expl[na]={};
//		expl[int(na*rnd())]=.3; // Deterministic exploration
//		for(int i=0;i<na;i++) expl[i]=.0003*tan(M_PI*(rnd()-.5)); // Tangent Distribution
//		for(int i=0;i<na;i++) expl[i]=.03*gauss(); // Gaussian distribution
		for(int i=0;i<na;i++) expl[i]=A_exp*rnd(); // Uniform distribution


		float Q=.001;
		for(int i=0;i<na;i++) { d1[i]=Q*rnd(); d2[i]=Q*rnd(); y[i]=Q*rnd(); }
		for(float t=0;t<T;t+=dt) bg_step(w1,w2,wm,x,y,dt,expl,k<500?1:1);

		for(int i=0;i<na;i++) cout<<y[i]<<'\t';
		for(int i=0;i<na;i++) cout<<d1[i]<<'\t';
		for(int i=0;i<na;i++) cout<<d2[i]<<'\t';
		for(int i=0;i<na;i++) cout<<gpe[i]<<'\t';
		for(int i=0;i<na;i++) cout<<gpi[i]<<'\t';
		cout<<endl;

		for(int i=0;i<na;i++) cerr<<w1[25][i]<<'\t';
		for(int i=0;i<na;i++) cerr<<w2[25][i]<<'\t';
		for(int i=0;i<nc;i++) cerr<<wm[25][i]<<'\t';
		cerr<<endl;
		
		float phi[4]={};
		phi[0]=phi0[0]; phi[1]=phi0[1]; phi[2]=0; phi[3]=0;
		float S=2,Y[na],th=.001;
//		for(int i=0;i<na;i++) Y[i]=y[i]>th?y[i]*S:0;
		for(int i=0;i<na;i++) Y[i]=(y[i]-th)*S;
		reach(phi,Y);
		float xcur=(-L1*sin(phi[0])+-L2*sin(phi[1])),ycur=(L1*cos(phi[0])+L2*cos(phi[1]));

		float target=((k/500)%2==0)?25:50;
//		float target=int(k/250)*25-12.5;

		float A=.2;
		float x0=xc+A*cos(2*M_PI*target/na),y0=yc+A*sin(2*M_PI*target/na);

		float d=hypot(xcur-x0,ycur-y0);

		out<<xcur<<'\t'<<ycur<<'\t'<<x0<<'\t'<<y0<<'\t'<<d<<'\t'<<xc<<'\t'<<yc<<endl;

//		float R=-d*d*10;
		float R=(d>.2?0:(d>.1?1:(d>.05?2:3)));
//		float R=(d>.05?0:3);
		
		bg_learn(w1,w2,x,y,(R-Rpre),wm);
		
		Rpre=R;
	}

	for(int j=0;j<nc;j++)
	{
		delete w1[j];
		delete w2[j];
		delete wm[j];
	}
	return 0;
}

