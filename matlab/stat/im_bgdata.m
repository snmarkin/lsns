function dset = im_bgdata(filename,para,opt)

% im_bgdata import BG data.
%
%   im_bgdata(filename,para,opt) imports BG data stored in filename.txt
%   with parameters in a structure and 3 options into a stucture called
%   dset
%
%   This function needs 3 arguments:filename, para, opt
%   1. filename: the text filename to import
%   2. para: parameters stored in a structure
%           para.m: the number of row per data type
%           para.n: the number of colum per data type
%           (para.m*para.n will be calculated to get the total number of
%            data per a data type.)
%           para.tr: the number of trials per experiment
%           para.tp: the number of different types of data
%           para.optn: the number of data types per row in pos.txt for
%           option 1
%           
%   3. opt: the option number. 0: for dat.txt www.txt, 1:for pos.txt
%
%   d = im_bgdata(...) returns a structure.
%
% Programmed by TAEGYO KIM, BME Ph.D
% Rybak Lab, Drexel University, Philadelphia, US
% 6.13. 2013

if nargin == 1
    para.m = 1;
    para.n = 100;
    para.tr = 100;
    para.tp = 5;
    para.optn = 1;
    opt = 0;
elseif nargin == 2
    opt = 0;
end

n = para.n*para.m;

fid = fopen(filename,'r');
scdata  = textscan(fid,'%f','delimiter',sprintf('\t'));

% scdata  = textscan(fid,'%f','delimiter',sprintf('\t'));
fclose(fid);

k = para.tr;

if opt==0
    tp = para.tp;
    for ik = 1:k
        for it = 1:tp
            for in = 1:n
               dset{ik}(it,in) = scdata{1}((ik-1)*tp*n+(it-1)*n+in);
            end
        end
    end
elseif opt == 1
    tp = para.tp;
    n = para.optn*para.m;
    for ik = 1:k
       for it = 1:tp
          for in = 1:n
               dset{ik}(it,in) = scdata{1}((ik-1)*tp*n+(it-1)*n+in);
          end
       end
    end    
elseif opt == 2
    tp = para.tp;
    for ik = 1:k
       for it = 1:tp
           dset(ik, it) = scdata{1}((ik-1)*tp + it);
       end
    end  
end