% Gutierrez simulation with TAN 3.29.2017
% Galea simulation with TAN 4.10.2017

clear
clc

%% Color setting

color(1,:) = [0.2 0.2 1]; % navy blue
color(2,:) = [0.9 0 0]; % dark red
color(3,:) = [0 0.8 0]; % dark green
color(4,:) = [0.8 0.4 0]; % brown
color(5,:) = [0 0.6 0.8]; % sky blue
color(6,:) = [1 0.3 0]; % orange
color(7,:) = [0.8 0 0.8]; % violet
color(8,:) = [0.8 0.8 0]; % gold
color(9,:) = [0.8 0.5 0.6]; % pink
color(10,:) = [0.2 0.2 1];
color(11,:) = [0.9 0 0];
color(12,:) = [0 0.8 0];
color(13,:) = [0.2 0.2 0.2]; % dark gray
color(14,:) = [0.8 0.8 0.8]; % light gray
color(15,:) = [0 0 0]; % black
color(16,:) = [1 1 1]; % white
color(15,:) = [0.9 0.7 0.7];

%% Setting

GaleaOn = 0;
GaleaDeg = 0;

std_error = 1;

DWP = 3; %1:D (data), 2:W (www), 3:P (pos)
% dataN = 1; %data to extract
saveflg = 0;
loadpreData = 0;

savepathname = 'C:\Projects\VC2010\OpenLSNS.v1.2.1\results\';

pathname = 'C:\Projects\VC2010\OpenLSNS.v1.2.1\';
% pathname = 'C:\Users\Taegyo\Desktop\LinuxShare\temp\';
% pathname = 'C:\Users\Taegyo\Desktop\LinuxShare\GutierrezWOTan\';
% pathname = 'C:\Users\Taegyo\Desktop\LinuxShare\GaleaWOTan\';

% GaleaWOTan: N to W, preWMval=0.33, Rrad = 0.13, inhy=1.5
%TAN1: TAN step = BG step, TAN2: TAN step = 10 BG step
%m1x: m1 receptor off, m1o: m1 receptor on
%(ss5,ls5,tt3,ta10)CTX: CTX=1 input to TAN at starting step 5, step length
%5, tau_tan 3, tau_ahp 10.

DataName = 'df_Ex100_tr150_w';
% DataName = 'Galea_Ex8_tr49_wTANR2_scl(0.0167)_1(sn60,ss5,ls5,tt3,ta10)CTX';
% DataName = 'Gutierrez_Ex8_tr75_wTANR2_scl(0.0167)_1(sn60,ss5,ls5,tt3,ta10)CTX';
% DataName = 'Gutierrez_Ex8_tr75_wTANR2_scl(0.01)_1(sn100,ss5,ls15,tt2,ta80)CTX';

if DWP == 1
    preDataName = [DataName '_data.mat'];
    param.tp = 5;
    dataN = [1 2 3 4 5]; %data to extract
elseif DWP == 2
    preDataName = [DataName '_www.mat'];
%     param.tp = 4;
%     dataN = [1 2 3 4]; %data to extract
    param.tp = 6;
    dataN = [1 2 3 4 5 6]; %data to extract
elseif DWP == 3
    preDataName = [DataName '_pos.mat'];
    param.tp = 1;
    dataN = [1]; %data to extract
end



param.m = 1;
param.n = 100;
% param.optn = 13;
param.optn = 14;

tr_id = strfind(DataName,'tr');
tmpid = strfind(DataName(tr_id:length(DataName)),'_');
if isempty(tr_id) == 0
    param.tr = str2num(DataName(tr_id+2:tr_id+tmpid(1)-2));
%     param.tr = str2num(DataName(tr_id+2:length(DataName)));
else
    param.tr = 75;
end
clear tr_id tmpid;

% param.tr = 500; %Trial

param.chng_start = 25;
param.chng_end = 75;

param.init_val = 25;
param.chng_val = 75;

%% File name setting

Ex_id = strfind(DataName,'Ex');
tmpid = strfind(DataName(Ex_id(1):length(DataName)),'_');
if isempty(tmpid) == 0
    fn = str2num(DataName(Ex_id(1)+2:Ex_id(1)+tmpid(1)-2));
else
    fn = 10;
end
clear tr_id tmpid Ex_id;

% fn = 10;

%% Import bgdata

if loadpreData == 0
    disp('=========== Importing multiple BG data ===========');
    disp(['The directory path = ' pathname]);
    for f=1:fn
        if DWP == 1
            filename = ['dat' num2str(f) '.txt'];
        elseif DWP == 2
            filename = ['www' num2str(f) '.txt'];
        elseif DWP == 3
            filename = ['pos' num2str(f) '.txt'];
        end
        pfilename = [pathname filename];
        disp(['Importing data from ' filename])
        if DWP == 3
            data = im_bgdata(pfilename,param,1);
        else
            data = im_bgdata(pfilename,param,0);
        end
        

        dt(f).d = data;
        clear data;
    end
else
   load(preDataName);
   disp(['Pre-processed data, ' preDataName ' were loaded']);
end

disp('=========== Processing BG data ===========');
%% Processing
k=param.tr;
if DWP == 3
    n=param.optn;
else
    n=param.n;
end
nd=length(dataN);
if loadpreData == 0
    for dd=1:nd
        for f=1:fn
            disp(['Processing data file [' num2str(f) '] ... ']);         
            for i=1:k
                    d(f,i,:) = dt(f).d{i}(dataN(dd),:);
            end
        end
        
        for i=1:n
            tmp = d(:,:,i);
            dw(dd).dt(i).d = tmp;
        end
        
        for i=1:k
           for j=1:n
               d_avg(i,j) = mean(d(:,i,j));
               if std_error == 1
                   d_std(i,j) = std(d(:,i,j))/sqrt(fn);
               else
                   d_std(i,j) = std(d(:,i,j));
               end
           end
        end
        dw(dd).d_avg = d_avg;
        dw(dd).d_std = d_std;
    
    end
    
    clear f i j A B d dd nd d_avg d_std tmp;
    
    if (saveflg == 1)
        save([savepathname preDataName]);
        disp(['All processed data were saved into ' preDataName]);
    end
end

clear k n type;

%% Data processing for Galea
if GaleaOn == 1
    if GaleaDeg == 1
        for f=1:fn
            for i=1:param.tr
                xc = dw.dt(9).d(f,i);
                yc = dw.dt(10).d(f,i);
                x0 = dw.dt(4).d(f,i);
                y0 = dw.dt(5).d(f,i);
                xcr = dw.dt(2).d(f,i);
                ycr = dw.dt(3).d(f,i);

                Dtrg = calDegE0(x0,y0,xc,yc);
                Dact = calDegE0(xcr,ycr,xc,yc);
                Derr(f,i) = Dtrg-Dact;
            end
        end
        for i=1:param.tr
            Derr_avg(i) = mean(Derr(:,i));
           if std_error == 1
                Derr_std(i) = std(Derr(:,i))/sqrt(fn);
            else
                Derr_std(i) = std(Derr(:,i));
            end
        end
    elseif GaleaDeg == 0
        for f=1:fn
            for i=1:param.tr
                x0 = dw.dt(4).d(f,i);
                xcr = dw.dt(2).d(f,i);

                Derr(f,i) = xcr-x0;
            end
        end
        for i=1:param.tr
            Derr_avg(i) = mean(Derr(:,i));
            if std_error == 1
                Derr_std(i) = std(Derr(:,i))/sqrt(fn);
            else
                Derr_std(i) = std(Derr(:,i));
            end
        end
    end
     clear Dtrg Dact f i xc yc xcr ycr x0 y0 dt;
end


%% Ploting

if GaleaOn == 1
    plot_Gale;
else
    plot_Guti;
end


