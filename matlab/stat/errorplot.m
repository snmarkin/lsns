
function p = errorplot(x,y,e,colorcode,transparency)

% ERRORPLOT Liniear plot with error boundaries.
%
%   ERROPLOT(X,Y,E) plots vector Y versus vector X with vector E which is
%   the standard deviation of Y. The boundaries are calculated by Y+E and
%   Y-E internally and plotted. The space between two boundaries is filled
%   with the pale color calculated from the specific input color. For
%   multiple plottings, color changes and color transparency control are
%   available with optional input arguments.
%
%   This function needs 5 arguments: X, Y, E, colorcode, transparency
%   1. X: the input vector
%   2. Y: the output vector
%   3. E: the standard deviation of the vector Y
%   4. colorcode: optional color vector ex) [0 0 0.8]
%   5. transparency: optional value to adjust transparency of error fill
%      between 1 - 0
%
%   p = errorplot(...) returns a vector of plot handle.
%
%      For example,
%      x = 1:0.1:100;
%      y = sin(x);
%      e = std(y)*ones(size(x));
%      errorplot(x,y,e)
%
%   See also PATCH, ALPHA, PLOT, ERRORBAR, AREA
%
% Programmed by TAEGYO KIM, BME Ph.D
% Giszter Lab, Drexel University, Philadelphia, US
% 2.11. 2013

if nargin == 5
    trpc = transparency;
elseif nargin == 4
    trpc = 1;
elseif nargin == 3
    trpc = 1;
    colorcode = [0 0 0.8]; % navy blue
else
    disp('ERROR: not enough input arguments!');
    return;   
end

%% Calculating the pale color from the input color for error fill

colorcode2 = [1 1 1];
maxc = max(colorcode);
tempp = find(colorcode == maxc);
minc = min(colorcode);
temp2p = find(colorcode == minc);
if length(tempp) > 1
    midc = maxc;
    midp = tempp(2);
    maxp = tempp(1);
    minp = temp2p;
else
    maxp = tempp;
    if length(temp2p) > 1
        midc = minc;
        midp = temp2p(1);
        minp = temp2p(2);
    else
        midc = median(colorcode);
        midp = find(colorcode == midc);
        minp = temp2p;
    end     
end

if maxc < 0.9
    dff = 0.9 - maxc;
    colorcode2(maxp) = 0.9;
    if length(tempp) == 2
        colorcode2(midp) = 0.9;
        if minc == 0
            colorcode2(minp) = 0.8;
        else
            colorcode2(minp) = minc + dff;
        end
    else
        if length(temp2p) == 2
            if minc == 0
                colorcode2(midp) = 0.8;
                colorcode2(minp) = 0.8;
            else
                colorcode2(midp) = minc + dff;
                colorcode2(minp) = minc + dff;
            end
        else
            colorcode2(midp) = midc + dff;
            if minc == 0
                colorcode2(minp) = midc + dff - 0.1;
            else
                colorcode2(minp) = minc + dff;
            end
        end
        
    end
else
    colorcode2(maxp) = maxc;
    if length(tempp) == 2
        colorcode2(midp) = maxc;
        if minc == 0
            colorcode2(minp) = 0.8;
        else
            colorcode2(minp) = minc + dff;
        end
    else
        if length(temp2p) == 2
            if minc == 0
                colorcode2(midp) = 0.8;
                colorcode2(minp) = 0.8;
            else
                colorcode2(midp) = minc;
                colorcode2(minp) = minc;
            end
        else
            colorcode2(midp) = midc;
            if minc == 0
                colorcode2(minp) = midc - 0.1;
            else
                colorcode2(minp) = minc;
            end
        end
        
    end
end

%% Plotting

yen = y-e;
lengx = length(x);

p1 = patch([x x(lengx:-1:1)],[y+e yen(lengx:-1:1)],colorcode2); set(p1,'LineStyle','none'); 
hold on;
alpha(trpc);
p = plot(x,y,'color',colorcode,'LineWidth',1.5);
hold off;

clear colorcode2 yen p1 trpc maxc midc minc maxp midp minp tempp temp2p;
clear x y e colorcode transparency;

end